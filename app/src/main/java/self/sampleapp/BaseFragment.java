package self.sampleapp;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;

/**
 * Created by srikrishna on 16/07/17.
 */

public class BaseFragment extends Fragment {
    protected ProgressDialog pDialog;
    public void showProgress(String message){
        if(getActivity() == null)
            return;
        if(pDialog != null && pDialog.isShowing()){
            pDialog.hide();
            pDialog = null;
        }
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(message);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void hideProgress(){
        if(getActivity() == null)
            return;
        if(pDialog != null && pDialog.isShowing())
            pDialog.hide();
        pDialog = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgress();
    }
}
