package self.sampleapp.fragments;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.sampleapp.App;
import self.sampleapp.R;
import self.sampleapp.model.Complaint;
import self.sampleapp.model.ComplaintDO;
import self.sampleapp.model.Complaints;
import self.sampleapp.web.WebAPI;

/**
 * Created by Gopi on 11/16/2016.
 */

public class ComplaintsFragment extends Fragment {
    private TabLayout tlCompliantsTab;
    private ViewPager vpCompliant;

    private FloatingActionButton createComplaintFab, createSuggestionFab, createResquestFab, fab;
    private LinearLayout createComplaintLayout, createSuggestionLayout, createRequestLayout;
    private boolean isFabMenuOpen = false, isFirstDocUpload = false;
    private Toolbar toolbar;
    private Animation fabOpenAnimation, fabCloseAnimation;
//    private MaterialSearchView searchView;
    ArrayList<ComplaintDO> iTunesItemList;
    ArrayList<Complaint> listNewComplaint = new ArrayList<>();
    ArrayList<Complaint> listOnGoingComplaint = new ArrayList<>();
    @Inject WebAPI mWebAPI;
    @Inject
    SharedPreferences sPref;
    private CompositeDisposable disposables = new CompositeDisposable();

    public static ComplaintsFragment newInstance() {
        ComplaintsFragment fragment = new ComplaintsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.compliants_container_fragment, container, false);

//        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        getAnimations();
        ButterKnife.bind(this, view);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
        initializeControls(view);
        bindViewControls();

        return view;
    }

    public void setData(ArrayList<ComplaintDO> iTunesItemList)
    {
        this.iTunesItemList = iTunesItemList;
    }

    private void initializeControls(View view) {
        tlCompliantsTab       = (TabLayout) view.findViewById(R.id.tlCompliantsTab);
        vpCompliant           = (ViewPager) view.findViewById(R.id.vpCompliant);
    }
    private void bindViewControls(){
//        callWebForContent();
        setupViewPager(vpCompliant);
        vpCompliant.setCurrentItem(0);
        tlCompliantsTab.setupWithViewPager(vpCompliant);
        tlCompliantsTab.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
        tlCompliantsTab.setSelectedTabIndicatorHeight(10);
        tlCompliantsTab.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(CompliantsOnGoingFragment.newInstance(listOnGoingComplaint), "ONGOING");
        adapter.addFragment(CompliantsNewFragment.newInstance(listNewComplaint), "NEW");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void callWebForContent(){

        String projectId = sPref.getString("bmaUserId", "");

        Observable<Complaints> callEOs = mWebAPI.getTechnicianComplaintList(/*projectId*/"5");
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processNewComplaints(x), x -> onError(x));
        disposables.add(callLogin);

//        String projectId = sPref.getString("bmaUserId", "");



    }

    private void processNewComplaints(Complaints complaints){
//        ArrayList<Notice> notices = complaints.getNotices();
//        boolean isNonEmpty = notices != null && notices.size() > 0;
//        rvExOffers.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
//        tvNoExOf.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
//        aCommunityUpdates.refresh(notices);

        if (complaints != null && complaints.getComplaints() != null && complaints.getComplaints().size() > 0) {
            listNewComplaint = complaints.getComplaints();
//            mAdapter = new ComplaintsAdapter(complaints.getComplaints(),getActivity());
//            rvComplaints.setAdapter(mAdapter);

//            llAvailability.setVisibility(View.GONE);
//            tvNotAvailable.setText("No Request Found");
//            rvComplaints.setVisibility(View.VISIBLE);
        }/*else {
            llAvailability.setVisibility(View.VISIBLE);
            tvNotAvailable.setText("No Request Found");
            rvComplaints.setVisibility(View.GONE);
        }*/
        callgetOnGoingComplaint();
    }

    private void callgetOnGoingComplaint(){

        String projectId = sPref.getString("bmaUserId", "");

        Observable<Complaints> callEOs = mWebAPI.getTechnicianOnGoingComplaintList(/*projectId*/"5");
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processOnGoingComplaints(x), x -> onError(x));
        disposables.add(callLogin);

    }

    private void processOnGoingComplaints(Complaints complaints){
//        ArrayList<Notice> notices = complaints.getNotices();
//        boolean isNonEmpty = notices != null && notices.size() > 0;
//        rvExOffers.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
//        tvNoExOf.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
//        aCommunityUpdates.refresh(notices);

        if (complaints != null && complaints.getComplaints() != null && complaints.getComplaints().size() > 0) {
            listOnGoingComplaint = complaints.getComplaints();
//            mAdapter = new ComplaintsOnGoingAdapter(complaints.getComplaints(),getActivity());
//            rvComplaints.setAdapter(mAdapter);

//            llAvailability.setVisibility(View.GONE);
//            tvNotAvailable.setText("No Request Found");
//            rvComplaints.setVisibility(View.VISIBLE);
        }/*else {
            llAvailability.setVisibility(View.VISIBLE);
            tvNotAvailable.setText("No Request Found");
            rvComplaints.setVisibility(View.GONE);
        }*/

    }

    private void onError(Throwable throwable) {
//        Snackbar.make(etLogin, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }
}