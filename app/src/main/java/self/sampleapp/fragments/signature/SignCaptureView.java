/*
 * Copyright (C) 2013 - Cognizant Technology Solutions. 
 * This file is a part of OneMobileStudio 
 * Licensed under the OneMobileStudio, Cognizant Technology Solutions, 
 * Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *      http://www.cognizant.com/
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package self.sampleapp.fragments.signature;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


import java.io.IOException;

import self.sampleapp.R;

/**
 * SignCaptureView : Draws Signature on canvas and converts to a view.
 * 
 * @author 280779
 * 
 */
public class SignCaptureView extends View {

	private final String TAG = this.getClass().getSimpleName();
	private static final int DEFAULT_HEIGHT = 200;
	private static final int DEFAULT_WIDTH = 200;
	private static final int STROKE_DEFAULT_WIDTH = 5;
	private static final int STROKE_DEFAULT_COLOR = Color.BLACK;
	private static final int BG_DEFAULT_COLOR = 0xFFFFFFFF;
	private static final float TOUCH_TOLERANCE = 2;
	private int viewHeight;
	private int viewWidth;
	private Paint paint;
	private Path path;
	private Bitmap bitmap;
	private Canvas canvas;
	private Paint bitmapPaint;
	private float mX, mY;
	private float mStrokeWidth = STROKE_DEFAULT_WIDTH;
	private int mStrokeColor = STROKE_DEFAULT_COLOR;
	private int mCanvasBgColor = BG_DEFAULT_COLOR;
	private boolean isWidthSet = false;
	private boolean isHeightSet = false;
	private boolean isSigned = false;

	public SignCaptureView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setDefaultSettings();
		setAttributes(attrs);
	}

	public SignCaptureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setDefaultSettings();
		setAttributes(attrs);
	}

	public SignCaptureView(Context context) {
		super(context);
		setDefaultSettings();
		setAttributes(null);
	}

	public void setView(String imageData) {
		try {
			bitmap = BitmapFactory.decodeByteArray(Base64.decode(imageData, Base64.DEFAULT),
					0, Base64.decode(imageData, Base64.DEFAULT).length);
		} catch (Exception e) {
			Log.e(TAG, "Error Occured in SetView -" + e.getMessage());
			e.printStackTrace();
		}
	}

	private void setAttributes(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.Signature);
			String signatureStrokeWidth = a
					.getString(R.styleable.Signature_stroke_width);
			Log.d(TAG, "Signature_stroke_width : " + signatureStrokeWidth);
			if (signatureStrokeWidth != null)
				mStrokeWidth = Float.parseFloat(signatureStrokeWidth);
			mStrokeColor = a.getColor(R.styleable.Signature_stroke_color,
					STROKE_DEFAULT_COLOR);
			mCanvasBgColor = a.getColor(R.styleable.Signature_background_color,
					BG_DEFAULT_COLOR);
			
			a.recycle();
		}
		setStrokeWidth(mStrokeWidth);
		setStrokeColor(mStrokeColor);
		setBackgroundColor(mCanvasBgColor);
	}

	public void setStrokeColor(int color) {
		mStrokeColor = color;
		paint.setColor(color);
	}

	public void setStrokeWidth(Float paintWidth) {
		mStrokeWidth = paintWidth;
		paint.setStrokeWidth(paintWidth);
	}

	@Override
	public void setBackgroundColor(int color) {
		mCanvasBgColor = color;
	}

	public void setHeight(int height) {
		viewHeight = height;
		isHeightSet = true;
	}

	public void setWidth(int width) {
		viewWidth = width;
		isWidthSet = true;
	}

	public void clear() {
		paint.setColor(mStrokeColor);
		if (canvas != null) {
			bitmapPaint.setColor(mCanvasBgColor);
			canvas.drawPaint(bitmapPaint);
			invalidate();
		}
	}

	public Bitmap getSignatureBitmap() {
		paint.setColor(Color.TRANSPARENT);
		return this.getDrawingCache(true);
	}

	private void setDefaultSettings() {
		path = new Path();
		bitmapPaint = new Paint(Paint.DITHER_FLAG);
		intializePaint();
		setDrawingCacheEnabled(true);
		setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
	}

	private void intializePaint() {
		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setDither(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeCap(Paint.Cap.ROUND);
	}

	@Override
	protected void onSizeChanged(int width, int height, int oldWidth,
			int oldHeight) {
		super.onSizeChanged(width, height, oldWidth, oldHeight);
		Log.d(TAG, "onSizeChanged, w, h" + width + " , " + height);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(mCanvasBgColor);
		canvas.drawBitmap(bitmap, 0, 0, bitmapPaint);
		canvas.drawPath(path, paint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();
		isSigned = true;
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touchStart(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_MOVE:
			touchMove(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_UP:
			touchUp(x, y);
			invalidate();
			break;
		}
		return true;
	}

	private void touchStart(float x, float y) {
		path.reset();
		path.moveTo(x, y);
		mX = x;
		mY = y;
	}

	private void touchMove(float x, float y) {
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
			mX = x;
			mY = y;
		}
	}

	private void touchUp(float x, float y) {
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if (dx == 0 && dy == 0) {
			path.lineTo(x + 1, y + 1);
		}
		path.lineTo(mX, mY);
		canvas.drawPath(path, paint);
		path.reset();
	}

	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		Log.d("SignatureCapture", "onMeasure method");
		if (isWidthSet == false) {
			viewWidth = measureWidth(widthMeasureSpec);
		}
		if (isHeightSet == false) {
			viewHeight = measureHeight(heightMeasureSpec);
		}
		bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		setMeasuredDimension(viewWidth, viewHeight);

	}

	private int measureWidth(int measureSpec) {
		int preferred = DEFAULT_WIDTH;
		return getMeasurement(measureSpec, preferred);
	}

	private int measureHeight(int measureSpec) {
		int preferred = DEFAULT_HEIGHT;
		return getMeasurement(measureSpec, preferred);
	}

	private int getMeasurement(int measureSpec, int preferred) {
		int specSize = MeasureSpec.getSize(measureSpec);
		int measurement = 0;

		switch (MeasureSpec.getMode(measureSpec)) {
		case MeasureSpec.EXACTLY:
			// This means the width of this view has been given.
			measurement = specSize;
			break;
		case MeasureSpec.AT_MOST:
			// Take the minimum of the preferred size and what
			// we were told to be.
			measurement = Math.min(preferred, specSize);
			break;
		default:
			measurement = preferred;
			break;
		}

		return measurement;
	}

	public boolean isSigned() {
		return isSigned;
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		// The vars you want to save - in this instance a string and a boolean
		String tempString = "something";
		boolean someBoolean = true;
		State state = new State(super.onSaveInstanceState(), tempString,
				someBoolean);
		bundle.putParcelable(State.STATE, state);
		return bundle;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			State customViewState = (State) bundle.getParcelable(State.STATE);
			super.onRestoreInstanceState(customViewState.getSuperState());
			return;
		}
		// Stops a bug with the wrong state being passed to the super
		super.onRestoreInstanceState(BaseSavedState.EMPTY_STATE);
	}

	protected static class State extends BaseSavedState {
		protected static final String STATE = "YourCustomView.STATE";

		private final String someText;
		private final boolean somethingShowing;

		public State(Parcelable superState, String someText,
                     boolean somethingShowing) {
			super(superState);
			this.someText = someText;
			this.somethingShowing = somethingShowing;
		}

		public String getText() {
			return this.someText;
		}

		public boolean isSomethingShowing() {
			return this.somethingShowing;
		}
	}
}
