/*
 * Copyright (C) 2013 - Cognizant Technology Solutions. 
 * This file is a part of OneMobileStudio 
 * Licensed under the OneMobileStudio, Cognizant Technology Solutions, 
 * Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *      http://www.cognizant.com/
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package self.sampleapp.fragments.signature;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

import self.sampleapp.R;

/**
 * Signature : 1) Captures signature and stores the view into a Bitmap image 2)
 * Converts the image to a encoded String and sends as a result to the Caller.
 * 
 * @author 280779
 */
public class Signature extends Activity {
	private final String TAG = this.getClass().getSimpleName();
	private static final int SMALL_STROKE = 3;
	private static final int MEDIUM_STROKE = 5;
	private static final int LARGE_STROKE = 10;
	protected static final String CLEAR_STATE = "clear";
	protected static final String IMAGE_EXTRA = "IMAGE_STRING";
	private static final String SAVE_STATE = "save";
	protected static final int SIGNATURE_RESULTCODE = 1666;
	// The view responsible for drawing the window.
	private SignCaptureView signatureView;
	private TextView enterText;
	private Button small, medium, large, drawButton;
	private String imageData = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signature_main_layout);
		// Use this code to include custom view from Layout file XML
		signatureView = (SignCaptureView) findViewById(R.id.mySign);
		enterText = (TextView) findViewById(R.id.enterString);
		Button clear = (Button) findViewById(R.id.clear);
		clear.setOnClickListener(clearListener);
		small = (Button) findViewById(R.id.s);
		small.setOnClickListener(sListener);
		medium = (Button) findViewById(R.id.m);
		medium.setOnClickListener(mListener);
		large = (Button) findViewById(R.id.l);
		large.setOnClickListener(lListener);
		drawButton = (Button) findViewById(R.id.draw);
		drawButton.setOnClickListener(drawListener);
		updateScreen(CLEAR_STATE);
		if (savedInstanceState != null) {
			signatureView.setView(savedInstanceState.getString("signature"));
		}
	}

	OnClickListener clearListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			signatureView.clear();
			updateScreen(CLEAR_STATE);
		}
	};
	OnClickListener sListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			signatureView.setStrokeWidth((float) SMALL_STROKE);

		}
	};
	OnClickListener mListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			signatureView.setStrokeWidth((float) MEDIUM_STROKE);

		}
	};
	OnClickListener lListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			signatureView.setStrokeWidth((float) LARGE_STROKE);
		}
	};

	OnClickListener drawListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {

			if (!signatureView.isSigned()) {
				/*Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.sign_proceed),
						Toast.LENGTH_LONG).show();*/
			} else {
				Bitmap bm = signatureView.getSignatureBitmap();
				Intent imageDataIntent = new Intent();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				byte[] b = baos.toByteArray();
				//imageData = OMSBase64.encodeBytes(b);
				imageDataIntent.putExtra(IMAGE_EXTRA, b);
				setResult(SIGNATURE_RESULTCODE, imageDataIntent);
				finish();
			}
		}
	};

	/**
	 * Updates the status.
	 * 
	 * @param state
	 */
	public void updateScreen(String state) {
		Log.d(TAG, "Update Screen Status: " + state);
		if (state.equalsIgnoreCase(SAVE_STATE)) {
			enterText.setText(R.string.saved);
			small.setEnabled(false);
			medium.setEnabled(false);
			large.setEnabled(false);
			drawButton.setEnabled(false);
			signatureView.setEnabled(false);
			signatureView.setClickable(false);
		} else {
			enterText.setText(R.string.enter);
			small.setEnabled(true);
			medium.setEnabled(true);
			large.setEnabled(true);
			drawButton.setEnabled(true);
			signatureView.setEnabled(true);
			signatureView.setClickable(true);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("signature", imageData);

	}

}
