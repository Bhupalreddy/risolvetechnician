package self.sampleapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import self.sampleapp.R;


/**
 * Created by srikrishna on 08-07-2017.
 */

public class ForgotPWDFragment extends Fragment {

    public static ForgotPWDFragment newInstance() {
        ForgotPWDFragment fragment = new ForgotPWDFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_pwd, container, false);
    }
}
