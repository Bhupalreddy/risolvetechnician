package self.sampleapp.fragments;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.sampleapp.App;
import self.sampleapp.R;
import self.sampleapp.adapters.ComplaintsOnGoingAdapter;
import self.sampleapp.model.Complaint;
import self.sampleapp.model.Complaints;
import self.sampleapp.web.WebAPI;


/**
 * Created by Gopi on 11/16/2016.
 */

@SuppressLint("ValidFragment")
public class CompliantsOnGoingFragment extends Fragment {

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
//    ArrayList<AllRequestsDO> listRequest;
    private Toolbar toolbar;
//    private Preference preference;
    private TextView tvSearch, tvNotAvailable;
    private String tabType = "";
    private LinearLayout llAvailability;
    private ArrayList<Complaint> listComplaint;
    @Inject
    WebAPI mWebAPI;
    @Inject
    SharedPreferences sPref;
    @BindView(R.id.rvComplaints) RecyclerView rvComplaints;
    private CompositeDisposable disposables = new CompositeDisposable();

    public static CompliantsOnGoingFragment newInstance(ArrayList<Complaint> listComplaint) {
        CompliantsOnGoingFragment fragment = new CompliantsOnGoingFragment(listComplaint);
        return fragment;
    }
    public CompliantsOnGoingFragment(ArrayList<Complaint> listComplaint) {
        this.listComplaint = listComplaint;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.complaint_fragment, container, false);
        ButterKnife.bind(this, view);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
//        preference = new Preference(getActivity());
//        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        toolbar.setTitle("Requests");
//        rvComplaints = (RecyclerView) view.findViewById(R.id.rvComplaints);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rvComplaints.setHasFixedSize(true);
//        tvSearch = (TextView) getActivity().findViewById(R.id.tvSearch);
//        tvSearch.setVisibility(View.VISIBLE);
//        llAvailability        =   (LinearLayout)  view.findViewById(R.id.llAvailability);
//        tvNotAvailable        =   (TextView)      view.findViewById(R.id.tvNotAvailable);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvComplaints.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        /*if(iTunesItemList==null)
        {
            iTunesItemList = new ArrayList<>();
            for(int i=0; i< 10;i++)
            {
                AssignmentItem item = new AssignmentItem();
                item.setAssignmentName("Assignment Name"+i);
                item.setAssignmentType("Type"+i);
                item.setAssignmentDept("Dept Name"+i);
                item.setAssignmentDate("12/27/2017");

                item.setAssignmentID("ID: 12345678");
                iTunesItemList.add(item);
            }
        }*/
//        mAdapter = new ComplaintsOnGoingAdapter(listComplaint, getActivity());
//        rvComplaints.setAdapter(mAdapter);
//        callAddRequestService("all");

        /*mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override public void onItemClick(View view, int position) {
                // TODO Handle item click
                Toast.makeText(getActivity(),"Item Clicked...", Toast.LENGTH_SHORT).show();
                AddRequestFragment addTicketFragment = new AddRequestFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("requestType", 1);
                bundle.putBoolean("isFromEdit", true);
                bundle.putSerializable("selectedReq", listRequest.get(position));
                addTicketFragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.container, addTicketFragment).commit();

            }
        }));*/
        return view;
    }


    /*private void callAddRequestService(String req) {

        String token = "Bearer "+preference.getStringFromPreference("Authorization", "");
        ((MainActivity)getActivity()).showLoader("");
        RestClient.getAPI(Constants.BASE_URL).getRequests(token, tabType, new RestCallback<ArrayList<AllRequestsDO>>() {
            @Override
            public void failure(RestError restError) {
                ((MainActivity)getActivity()).hideLoader();
            }

            @Override
            public void success(ArrayList<AllRequestsDO> responseDO, Response response) {
                ((MainActivity)getActivity()).hideLoader();
                if (responseDO != null && responseDO.size() > 0) {
                    listRequest = responseDO;
                    mAdapter = new RequestAdapter(responseDO,getActivity());
                    mRecyclerView.setAdapter(mAdapter);

                    llAvailability.setVisibility(View.GONE);
                    tvNotAvailable.setText("No Request Found");
                    mRecyclerView.setVisibility(View.VISIBLE);
                }else {
                    llAvailability.setVisibility(View.VISIBLE);
                    tvNotAvailable.setText("No Request Found");
                    mRecyclerView.setVisibility(View.GONE);
                }
            }
        });
    }*/

    @Override
    public void onStop() {
        super.onStop();
        Log.e("onStop()", "onStop() Called");
    }

    @Override
    public void onResume() {
        super.onResume();
        callWebForContent();
        Log.e("OnResume()", "OnResume() Called");
    }

    private void callWebForContent(){

        String projectId = sPref.getString("bmaUserId", "");

        Observable<Complaints> callEOs = mWebAPI.getTechnicianOnGoingComplaintList(projectId/*"5"*/);
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                .subscribe(x -> processEOs(x), x -> onError(x));
        disposables.add(callLogin);

    }

    private void processEOs(Complaints complaints){
//        ArrayList<Notice> notices = complaints.getNotices();
//        boolean isNonEmpty = notices != null && notices.size() > 0;
//        rvExOffers.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
//        tvNoExOf.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
//        aCommunityUpdates.refresh(notices);

        if (complaints != null && complaints.getComplaints() != null && complaints.getComplaints().size() > 0) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter = new ComplaintsOnGoingAdapter(complaints.getComplaints(),getActivity());
                    rvComplaints.setAdapter(mAdapter);
                }
            });

//            llAvailability.setVisibility(View.GONE);
//            tvNotAvailable.setText("No Request Found");
//            rvComplaints.setVisibility(View.VISIBLE);
        }/*else {
            llAvailability.setVisibility(View.VISIBLE);
            tvNotAvailable.setText("No Request Found");
            rvComplaints.setVisibility(View.GONE);
        }*/
    }

    private void onError(Throwable throwable) {
//        Snackbar.make(etLogin, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

   /* @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            callWebForContent();
            Log.e("isVisibleToUser()", "isVisibleToUser() Called");
        }
    }*/
}