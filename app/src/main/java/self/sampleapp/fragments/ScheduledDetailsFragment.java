package self.sampleapp.fragments;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import self.sampleapp.App;
import self.sampleapp.R;
import self.sampleapp.model.Schedule;
import self.sampleapp.web.WebAPI;


/**
 * Created by PsSri on 08-07-2017.
 */

public class ScheduledDetailsFragment extends Fragment implements View.OnClickListener{
    @BindView(R.id.et_schedule_tech_view_project) EditText projectNameEditText;
    @BindView(R.id.et_schedule_tech_view_subject) EditText subjectEditText;
    @BindView(R.id.et_schedule_tech_view_department) EditText departmentEditText;
    @BindView(R.id.et_schedule_tech_view_date) EditText dateEditText;
    @BindView(R.id.iv_schedule_tech_view_namecard)
    ImageView nameCardImageView;
    @BindView(R.id.tv_schedule_tech_view_contactdetails)
    TextView contactDetailsTextView;
    @BindView(R.id.ll_schedule_contactdetails)
    LinearLayout contactLinearLayout;
    @BindView(R.id.bt_schedule_ongoing_order)
    Button viewWorkOrderButton;
    @BindView(R.id.bt_schedule_ongoing_close_complaint)
    Button closeButton;

    //Web Service related
    @Inject
    SharedPreferences sPref;
    @Inject
    WebAPI mWebAPI;


    private CompositeDisposable disposables = new CompositeDisposable();

    private Schedule scheduleObject;
    private int containerID=-1;
    public static ScheduledDetailsFragment newInstance() {
        ScheduledDetailsFragment fragment = new ScheduledDetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args!=null)
        {
            scheduleObject = (Schedule) args.getSerializable("Schedule");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        containerID = container.getId();
        View rootView = inflater.inflate(R.layout.schedule_tech_view_complaint_fragment, container, false);
        ButterKnife.bind(this, rootView);
        ((App)getActivity().getApplication()).getNetComponent().inject(this);
        nameCardImageView.setOnClickListener(v->showAddressAlert());
        contactDetailsTextView.setOnClickListener(v->showAddressAlert());
        contactLinearLayout.setOnClickListener(v->showAddressAlert());
        closeButton.setOnClickListener(this);
        return rootView;
    }


    @Override
    public void onClick(View v) {
        Fragment selectedFragment =null;
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        switch (v.getId()){


            case R.id.bt_schedule_ongoing_order:
                break;
            case R.id.bt_schedule_ongoing_close_complaint:
                selectedFragment = SubmitServiceReportOrCloseFragment.newInstance();
                Bundle bundle4 = new Bundle();
                 bundle4.putString("COMPLAINT_ID", scheduleObject.getBma_schedule_id());
                selectedFragment.setArguments(bundle4);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                /*Intent signIntent = new Intent(getActivity(), Signature.class);
                getActivity().startActivityForResult(signIntent,1234);*/
                break;
        }
    }

    private void showAddressAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.user_contactdetails_custome_dialog,null);
        final EditText userNameEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_name);
        final EditText contactNoEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_mobile);
        final EditText addressEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_address);

        userNameEditText.setText(scheduleObject.getBma_inchrge_name());
        contactNoEditText.setText(scheduleObject.getBma_incharge_phone());
        addressEditText.setText(scheduleObject.getBma_incharge_email_id());

        builder.setView(dialog_layout)

                /*.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String quantity = Integer.parseInt(quantityEditText.getText().toString());
                        String itemname = itemNameEditText.getText().toString();
                        double unitprice = Double.parseDouble(unitPriceEditText.getText().toString());
                        double labourcharges = Double.parseDouble(labourChargesEditText.getText().toString());

                        EstimateMaterial item = new EstimateMaterial();
                        item.quantity = quantity;
                        item.itemName = itemname;
                        item.unitPrice = unitprice;
                        item.labourCharges = labourcharges;

                        items.add(item);
                        materialAdapter.notifyItemChanged(items.indexOf(item));
                        caluclateAndSetTotalCharges();
                        //materialAdapter.notifyDataSetChanged();

                    }
                })*/
                .setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
