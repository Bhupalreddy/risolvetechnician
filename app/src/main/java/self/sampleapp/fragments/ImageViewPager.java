package self.sampleapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import self.sampleapp.R;
import self.sampleapp.adapters.ViewPagerAdapter;


/*
 */
public class ImageViewPager extends Fragment {

    RecyclerView mRecyclerView;
    private double taxPercentage=14.5;
    private boolean showMenuAdd = true;
    private String screenTitle;
    private String[] imageURLS;

    ViewPager viewPager;
    PagerAdapter adapter;

    private TextView totalMaterialCostTextView;
    private TextView totalLabourChargesTextView;
    private TextView finalTotalTextView;
    private  TextView taxTextView;
    public static ImageViewPager newInstance() {
        ImageViewPager fragment = new ImageViewPager();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null)
        {
            screenTitle = bundle.getString("TITLE","View Pager");
            imageURLS = bundle.getStringArray("IMAGE_URLS");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.image_viewpager_main_layout, container, false);
// Locate the ViewPager in viewpager_main.xml
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        // Pass results to ViewPagerAdapter Class
        if(imageURLS!=null) {
            adapter = new ViewPagerAdapter(getActivity(), imageURLS);
            // Binds the Adapter to the ViewPager
            viewPager.setAdapter(adapter);
        }
        return rootView;
    }


}
