package self.sampleapp.fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import self.sampleapp.App;
import self.sampleapp.BaseFragment;
import self.sampleapp.ComplaintsActivity;
import self.sampleapp.R;
import self.sampleapp.Utils.GPSTracker;
import self.sampleapp.Utils.RisolveLocation;
import self.sampleapp.model.EstimateMaterial;
import self.sampleapp.model.EstimateService;
import self.sampleapp.model.EstimatedImageUploadRequestDO;
import self.sampleapp.model.PostEstimation;
import self.sampleapp.model.PostEstimationRequestDO;
import self.sampleapp.model.PostEstimationResponseDO;
import self.sampleapp.web.WebAPI;


/*
 */
public class OrderEstimatesFragment extends BaseFragment {
    @BindView(R.id.rvEstimatedItems)
    RecyclerView mRecyclerView;
    @BindView(R.id.rvEstimatedService)
    RecyclerView mServiceRecyclerView;
    private boolean showMenuAdd = true;
    private TextView totalMaterialCostTextView;
    private TextView totalLabourChargesTextView;
    private TextView finalTotalTextView;
    private String complaintID = "";
    private int containerID=-1;
    @Inject
    WebAPI mWebAPI;
    @Inject
    SharedPreferences sPref;
    private CompositeDisposable disposables = new CompositeDisposable();
    private Button submitButton;
    private String globalUrls = "";

    public static ArrayList<Uri> imageUri = new ArrayList<>();

    public static OrderEstimatesFragment newInstance() {
        OrderEstimatesFragment fragment = new OrderEstimatesFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if(bundle!=null)
        {
            showMenuAdd = bundle.getBoolean("SHOW_ADD",false);
            complaintID = bundle.getString("complaintID","");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.estimation_items_fragment_1, container, false);

        ButterKnife.bind(this, rootView);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
        containerID = container.getId();
        totalLabourChargesTextView = (TextView) rootView.findViewById(R.id.tv_estimate_total_labor_charge);
        totalMaterialCostTextView = (TextView) rootView.findViewById(R.id.tv_estimate_total_Material);
        finalTotalTextView = (TextView) rootView.findViewById(R.id.tv_estimate_total);
        submitButton = (Button) rootView.findViewById(R.id.bt_estimate_submit);
        submitButton.setOnClickListener(v->callPostComplaintServices());
        ((ComplaintsActivity) getActivity()).setTitle("Order Estimation");
//        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvEstimatedItems);
//        mServiceRecyclerView = (RecyclerView) rootView.findViewById(R.id.rvEstimatedService);
        setHasOptionsMenu(true);
        setUpRecyclerView();
        setUpServiceRecyclerView();

        new RisolveLocation().getLocation(getActivity());
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.estimation_actions, menu);


    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if(!showMenuAdd) {
            MenuItem menuItem = menu.getItem(0);
            if(menuItem.getItemId() == R.id.action_add_item)
                menuItem.setVisible(false);
            submitButton.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_material:
                // do s.th.
                showAddMaterialAlert();
                return true;
            case R.id.action_add_service:
                // do s.th.
                showAddServiceAlert();
                return true;
            case R.id.action_add_image:
                Fragment selectedFragment = EstimationAddImagesFragment.newInstance();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showAddMaterialAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.add_estimate_item_custome_dialog,null);
        final EditText quantityEditText = (EditText) dialog_layout.findViewById(R.id.et_dialog_quantity);
        final EditText itemNameEditText = (EditText) dialog_layout.findViewById(R.id.et_dialog_itemname);
        final EditText unitPriceEditText = (EditText) dialog_layout.findViewById(R.id.et_dialog_unit_price);

        builder.setView(dialog_layout)

                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int quantity = Integer.parseInt(quantityEditText.getText().toString());
                        String itemname = itemNameEditText.getText().toString();
                        double unitprice = Double.parseDouble(unitPriceEditText.getText().toString());
                        //double labourcharges = Double.parseDouble(labourChargesEditText.getText().toString());

                        EstimateMaterial item = new EstimateMaterial();
                        item.quantity = quantity;
                        item.itemName = itemname;
                        item.unitPrice = unitprice;
                        item.amount = quantity*unitprice;

                        items.add(item);
                        materialAdapter.notifyItemChanged(items.indexOf(item));
                        caluclateAndSetTotalCharges();
                        //materialAdapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showAddServiceAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.add_estimate_service_item_custome_dialog,null);
        final EditText serviceNameEditText = (EditText) dialog_layout.findViewById(R.id.et_service_dialog_name);
        final EditText amountEditText = (EditText) dialog_layout.findViewById(R.id.et_service_dialog_amount);

        builder.setView(dialog_layout)

                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String serviceName = serviceNameEditText.getText().toString();
                        String amountText = amountEditText.getText().toString();

                        double amount=0.0;
                        if (!TextUtils.isEmpty(amountText))
                            amount = Double.parseDouble(amountEditText.getText().toString());
                        //double labourcharges = Double.parseDouble(labourChargesEditText.getText().toString());

                        EstimateService item = new EstimateService();
                        item.serviceName = serviceName;
                        item.amount = amount;

                        if(!TextUtils.isEmpty(serviceName)) {
                            serviceItems.add(item);
                            serviceAdapter.notifyItemChanged(serviceItems.indexOf(item));
                            caluclateAndSetTotalCharges();
                        }
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
// ----------- Material Items

    ArrayList<EstimateMaterial> items = new ArrayList<EstimateMaterial>();
    MaterialAdapter materialAdapter = new MaterialAdapter();
    private void setUpRecyclerView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //items = new ArrayList<EstimateMaterial>();
        if(showMenuAdd)
            materialAdapter.setUndoOn(true);
        else
            materialAdapter.setUndoOn(false);

        mRecyclerView.setAdapter(materialAdapter);
        mRecyclerView.setHasFixedSize(true);
        setUpItemTouchHelper(mRecyclerView);
        setUpAnimationDecoratorHelper(mRecyclerView);
    }
//--- service items
    ArrayList<EstimateService> serviceItems = new ArrayList<EstimateService>();
    ServiceAdapter serviceAdapter = new ServiceAdapter();
    private void setUpServiceRecyclerView() {
        mServiceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        serviceItems = new ArrayList<EstimateService>();
        if(showMenuAdd)
            serviceAdapter.setUndoOn(true);
        else
            serviceAdapter.setUndoOn(false);

        mServiceRecyclerView.setAdapter(serviceAdapter);
        mServiceRecyclerView.setHasFixedSize(true);
        setUpItemTouchHelper(mServiceRecyclerView);
        setUpAnimationDecoratorHelper(mServiceRecyclerView);
    }

    /**
     * This is the standard support library way of implementing "swipe to delete" feature. You can do custom drawing in onChildDraw method
     * but whatever you draw will disappear once the swipe is over, and while the items are animating to their new position the recycler view
     * background will be visible. That is rarely an desired effect.
     */
    private void setUpItemTouchHelper(RecyclerView recyclerView) {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            // we want to cache these and not allocate anything repeatedly in the onChildDraw method
            Drawable background;
            Drawable xMark;
            int xMarkMargin;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                xMark = ContextCompat.getDrawable(getActivity(), android.R.drawable.ic_menu_delete);
                xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) OrderEstimatesFragment.this.getResources().getDimension(R.dimen.activity_vertical_margin);
                initiated = true;
            }

            // not important, we don't want drag & drop
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                Object object = recyclerView.getBaseline();
                if(object instanceof MaterialAdapter) {
                    MaterialAdapter materialAdapter = (MaterialAdapter) recyclerView.getAdapter();
                    if (materialAdapter.isUndoOn() && materialAdapter.isPendingRemoval(position)) {
                        return 0;
                    }
                }
                if(object instanceof ServiceAdapter) {
                    ServiceAdapter serviceAdapter = (ServiceAdapter) recyclerView.getAdapter();
                    if (serviceAdapter.isUndoOn() && serviceAdapter.isPendingRemoval(position)) {
                        return 0;
                    }
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                Object object= recyclerView.getAdapter();
                boolean undoOn;
                if(object instanceof MaterialAdapter) {
                    MaterialAdapter adapter = (MaterialAdapter) recyclerView.getAdapter();
                    undoOn = adapter.isUndoOn();
                    if (undoOn) {
                        adapter.pendingRemoval(swipedPosition);
                    } else {
                        adapter.remove(swipedPosition);
                    }
                }
                if(object instanceof ServiceAdapter) {
                    ServiceAdapter adapter = (ServiceAdapter) recyclerView.getAdapter();
                    undoOn = adapter.isUndoOn();
                    if (undoOn) {
                        adapter.pendingRemoval(swipedPosition);
                    } else {
                        adapter.remove(swipedPosition);
                    }
                }

            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                View itemView = viewHolder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }

                if (!initiated) {
                    init();
                }

                // draw red background
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                // draw x mark
                int itemHeight = itemView.getBottom() - itemView.getTop();
                int intrinsicWidth = xMark.getIntrinsicWidth();
                int intrinsicHeight = xMark.getIntrinsicWidth();

                int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                int xMarkRight = itemView.getRight() - xMarkMargin;
                int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
                int xMarkBottom = xMarkTop + intrinsicHeight;
                xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

                xMark.draw(c);

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        };
        ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);
    }

    /**
     * We're gonna setup another ItemDecorator that will draw the red background in the empty space while the items are animating to thier new positions
     * after an item is removed.
     * @param recyclerView
     */
    private void setUpAnimationDecoratorHelper(RecyclerView recyclerView) {
        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {

            // we want to cache this and not allocate anything repeatedly in the onDraw method
            Drawable background;
            boolean initiated;

            private void init() {
                background = new ColorDrawable(Color.RED);
                initiated = true;
            }

            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {

                if (!initiated) {
                    init();
                }

                // only if animation is in progress
                if (parent.getItemAnimator().isRunning()) {

                    // some items might be animating down and some items might be animating up to close the gap left by the removed item
                    // this is not exclusive, both movement can be happening at the same time
                    // to reproduce this leave just enough items so the first one and the last one would be just a little off screen
                    // then remove one from the middle

                    // find first child with translationY > 0
                    // and last one with translationY < 0
                    // we're after a rect that is not covered in recycler-view views at this point in time
                    View lastViewComingDown = null;
                    View firstViewComingUp = null;

                    // this is fixed
                    int left = 0;
                    int right = parent.getWidth();

                    // this we need to find out
                    int top = 0;
                    int bottom = 0;

                    // find relevant translating views
                    int childCount = parent.getLayoutManager().getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View child = parent.getLayoutManager().getChildAt(i);
                        if (child.getTranslationY() < 0) {
                            // view is coming down
                            lastViewComingDown = child;
                        } else if (child.getTranslationY() > 0) {
                            // view is coming up
                            if (firstViewComingUp == null) {
                                firstViewComingUp = child;
                            }
                        }
                    }

                    if (lastViewComingDown != null && firstViewComingUp != null) {
                        // views are coming down AND going up to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    } else if (lastViewComingDown != null) {
                        // views are going down to fill the void
                        top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
                        bottom = lastViewComingDown.getBottom();
                    } else if (firstViewComingUp != null) {
                        // views are coming up to fill the void
                        top = firstViewComingUp.getTop();
                        bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
                    }

                    background.setBounds(left, top, right, bottom);
                    background.draw(c);

                }
                super.onDraw(c, parent, state);
            }

        });
    }

    /**
     * RecyclerView adapter enabling undo on a swiped away item.
     */
    class MaterialAdapter extends RecyclerView.Adapter {

        private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec

        // List<String> items;
        List<EstimateMaterial> itemsPendingRemoval;
        int lastInsertedIndex; // so we can add some more items for testing purposes
        boolean undoOn =true; // is undo on, you can turn it on from the toolbar menu

        private Handler handler = new Handler(); // hanlder for running delayed runnables
        HashMap<EstimateMaterial, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be

        public MaterialAdapter() {
            itemsPendingRemoval = new ArrayList<>();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MaterialViewHolder(parent);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            MaterialViewHolder viewHolder = (MaterialViewHolder)holder;
            final EstimateMaterial item = items.get(position);

            if (itemsPendingRemoval.contains(item)) {
                // we need to show the "undo" state of the row
                viewHolder.itemView.setBackgroundColor(Color.DKGRAY);
                viewHolder.qantityTextView.setVisibility(View.GONE);
                viewHolder.meterialTextView.setVisibility(View.GONE);
                viewHolder.unitPriceTextView.setVisibility(View.GONE);
                viewHolder.amountTextView.setVisibility(View.GONE);
                viewHolder.undoButton.setVisibility(View.VISIBLE);
                viewHolder.undoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // user wants to undo the removal, let's cancel the pending task
                        Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                        pendingRunnables.remove(item);
                        if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                        itemsPendingRemoval.remove(item);
                        // this will rebind the row in "normal" state
                        notifyItemChanged(items.indexOf(item));
                        caluclateAndSetTotalCharges();
                    }
                });
            } else {
                // we need to show the "normal" state
                viewHolder.itemView.setBackgroundColor(Color.WHITE);
                viewHolder.qantityTextView.setVisibility(View.VISIBLE);
                viewHolder.meterialTextView.setVisibility(View.VISIBLE);
                viewHolder.unitPriceTextView.setVisibility(View.VISIBLE);
                viewHolder.amountTextView.setVisibility(View.VISIBLE);
                viewHolder.qantityTextView.setText(""+item.quantity);
                viewHolder.meterialTextView.setText(item.itemName);
                viewHolder.unitPriceTextView.setText(String.valueOf(item.unitPrice));
                viewHolder.amountTextView.setText(String.valueOf(item.amount));
                viewHolder.undoButton.setVisibility(View.GONE);
                viewHolder.undoButton.setOnClickListener(null);
            }
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public void setUndoOn(boolean undoOn) {
            this.undoOn = undoOn;
        }

        public boolean isUndoOn() {
            return undoOn;
        }

        public void pendingRemoval(int position) {
            final EstimateMaterial item = items.get(position);
            if (!itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.add(item);
                // this will redraw row in "undo" state
                notifyItemChanged(position);
                caluclateAndSetTotalCharges();
                // let's create, store and post a runnable to remove the item
                Runnable pendingRemovalRunnable = new Runnable() {
                    @Override
                    public void run() {
                        remove(items.indexOf(item));
                    }
                };
                handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
                pendingRunnables.put(item, pendingRemovalRunnable);
            }
        }

        public void remove(int position) {
            EstimateMaterial item = items.get(position);
            if (itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.remove(item);
            }
            if (items.contains(item)) {
                items.remove(position);
                notifyItemRemoved(position);
                caluclateAndSetTotalCharges();
            }
        }

        public boolean isPendingRemoval(int position) {
            EstimateMaterial item = items.get(position);
            return itemsPendingRemoval.contains(item);
        }
    }

    /**
     * ViewHolder capable of presenting two states: "normal" and "undo" state.
     */
    static class MaterialViewHolder extends RecyclerView.ViewHolder {

        TextView qantityTextView;
        TextView meterialTextView;
        TextView unitPriceTextView;
        TextView amountTextView;
        Button undoButton;
        public MaterialViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.estimation_list_item_layout, parent, false));
            qantityTextView = (TextView) itemView.findViewById(R.id.tv_estimate_item_quantity);
            meterialTextView = (TextView) itemView.findViewById(R.id.tv_estimate_item_meterial);
            unitPriceTextView = (TextView) itemView.findViewById(R.id.tv_estimate_item_unitprice);
            amountTextView = (TextView) itemView.findViewById(R.id.tv_estimate_item_amount);
            undoButton = (Button) itemView.findViewById(R.id.undo_button);
        }

    }

    //----------- Service Items Adaper
    class ServiceAdapter extends RecyclerView.Adapter {

        private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec

        // List<String> items;
        List<EstimateService> itemsPendingRemoval;
        int lastInsertedIndex; // so we can add some more items for testing purposes
        boolean undoOn =true; // is undo on, you can turn it on from the toolbar menu

        private Handler handler = new Handler(); // hanlder for running delayed runnables
        HashMap<EstimateService, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be

        public ServiceAdapter() {
            itemsPendingRemoval = new ArrayList<>();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ServiceViewHolder(parent);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ServiceViewHolder serviceViewHolder = (ServiceViewHolder)holder;
            final EstimateService item = serviceItems.get(position);

            if (itemsPendingRemoval.contains(item)) {
                // we need to show the "undo" state of the row
                serviceViewHolder.itemView.setBackgroundColor(Color.DKGRAY);
                serviceViewHolder.serviceNameTextView.setVisibility(View.GONE);
                serviceViewHolder.amountTextView.setVisibility(View.GONE);
                serviceViewHolder.serviceUndoButton.setVisibility(View.VISIBLE);
                serviceViewHolder.serviceUndoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // user wants to undo the removal, let's cancel the pending task
                        Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                        pendingRunnables.remove(item);
                        if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                        itemsPendingRemoval.remove(item);
                        // this will rebind the row in "normal" state
                        notifyItemChanged(serviceItems.indexOf(item));
                        caluclateAndSetTotalCharges();
                    }
                });
            } else {
                // we need to show the "normal" state
                serviceViewHolder.itemView.setBackgroundColor(Color.WHITE);
                serviceViewHolder.serviceNameTextView.setVisibility(View.VISIBLE);
                serviceViewHolder.amountTextView.setVisibility(View.VISIBLE);
                serviceViewHolder.serviceNameTextView.setText(""+item.serviceName);
                serviceViewHolder.amountTextView.setText(String.valueOf(item.amount));
                serviceViewHolder.serviceUndoButton.setVisibility(View.GONE);
                serviceViewHolder.serviceUndoButton.setOnClickListener(null);
            }
        }

        @Override
        public int getItemCount() {
            return serviceItems.size();
        }

        public void setUndoOn(boolean undoOn) {
            this.undoOn = undoOn;
        }

        public boolean isUndoOn() {
            return undoOn;
        }

        public void pendingRemoval(int position) {
            final EstimateService item = serviceItems.get(position);
            if (!itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.add(item);
                // this will redraw row in "undo" state
                notifyItemChanged(position);
                caluclateAndSetTotalCharges();
                // let's create, store and post a runnable to remove the item
                Runnable pendingRemovalRunnable = new Runnable() {
                    @Override
                    public void run() {
                        remove(serviceItems.indexOf(item));
                    }
                };
                handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
                pendingRunnables.put(item, pendingRemovalRunnable);
            }
        }

        public void remove(int position) {
            EstimateService item = serviceItems.get(position);
            if (itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.remove(item);
            }
            if (serviceItems.contains(item)) {
                serviceItems.remove(position);
                notifyItemRemoved(position);
                caluclateAndSetTotalCharges();
            }
        }

        public boolean isPendingRemoval(int position) {
            EstimateService item = serviceItems.get(position);
            return itemsPendingRemoval.contains(item);
        }
    }

    /**
     * ViewHolder capable of presenting two states: "normal" and "undo" state.
     */
    static class ServiceViewHolder extends RecyclerView.ViewHolder {

        TextView serviceNameTextView;
        TextView amountTextView;
        Button serviceUndoButton;
        public ServiceViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.estimation_service_list_item_layout, parent, false));
            serviceNameTextView = (TextView) itemView.findViewById(R.id.tv_estimate_service_name);
            amountTextView = (TextView) itemView.findViewById(R.id.tv_estimate_service_amount);
            serviceUndoButton = (Button) itemView.findViewById(R.id.service_undo_button);
        }

    }
    ArrayList<PostEstimation> postEstimationlist = new ArrayList<PostEstimation>();
    private void caluclateAndSetTotalCharges()
    {
        double finalTotal =0.00;
        double totalServicecost =0.00;
        double totalMaterialcost=0.00;
        postEstimationlist.clear();
        if(items!=null && items.size()>0)
        {
            for(int i=0; i<items.size(); i++)
            {
                EstimateMaterial item = items.get(i);
                totalMaterialcost = totalMaterialcost+(item.quantity*item.unitPrice);
                PostEstimation postEstimation = new PostEstimation();
                postEstimation.qty = item.quantity;
                postEstimation.goods = item.itemName;
                postEstimation.unitPrice = item.unitPrice;
                postEstimation.goodsAmount = (item.quantity*item.unitPrice);
                postEstimation.service = "";
                postEstimationlist.add(postEstimation);
            }
        }
        if(serviceItems!=null && serviceItems.size()>0)
        {
            for(int i=0; i<serviceItems.size(); i++)
            {
                EstimateService item = serviceItems.get(i);
                totalServicecost = totalServicecost+ item.amount;
                PostEstimation postEstimation = new PostEstimation();
                postEstimation.service = item.serviceName;
                postEstimation.serviceAmount = item.amount;
                postEstimation.goods = "";
                postEstimationlist.add(postEstimation);
            }
        }
        finalTotal = totalServicecost + totalMaterialcost ;
        totalMaterialCostTextView.setText(""+totalMaterialcost);
        totalLabourChargesTextView.setText(""+totalServicecost);
        finalTotalTextView.setText(""+finalTotal);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new RisolveLocation().enableGPS(getActivity());
    }

    private Location getGPSLocation()
    {
        LocationManager locationManager = (LocationManager) getActivity().getApplication().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        Location location = null;
        String mprovider = locationManager.getBestProvider(criteria, false);

        if (mprovider != null && !mprovider.equals("")) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
             location = locationManager.getLastKnownLocation(mprovider);
        }
        return location;
    }


    private void callPostComplaintServices(){
        if (imageUri.size() > 0){
            callSendEstimateImages();
        }else{
            callWebForContent();
        }
    }
    private void callWebForContent(){

        PostEstimationRequestDO requestDO = new PostEstimationRequestDO();

        String techID = sPref.getString("bmaUserId", "");
        caluclateAndSetTotalCharges();
        String totalMaterialCost = totalMaterialCostTextView.getText().toString().trim();
        String totalLabourCost = totalLabourChargesTextView.getText().toString().trim();
        String totalAmt = finalTotalTextView.getText().toString().trim();
        GPSTracker location = new RisolveLocation().getLocation(getActivity());
        if(location==null)
            return;
        String latitude = String.valueOf(location.getLatitude());
        String longitude = String.valueOf(location.getLongitude());

        requestDO.setTech_id(techID);
        requestDO.setComplaint_id(complaintID);
        requestDO.setLatitude(latitude);
        requestDO.setLongitude(longitude);
        requestDO.setTotal(totalAmt);
        requestDO.setTotalgoodscast(totalMaterialCost);
        requestDO.setTotalservicecast(totalLabourCost);
        requestDO.setEstimation(postEstimationlist);
        if (globalUrls != null && !globalUrls.equalsIgnoreCase(""))
        requestDO.setImages(globalUrls);
        //Images data
        MediaType mediaType = MediaType.parse("");
        MultipartBody.Part[] imageFileParts = null;
        /*if (imageUri.size() > 0) {
            imageFileParts = new MultipartBody.Part[imageUri.size()];
            for (int i = 0; i < imageUri.size(); i++) {
                File file = new File(imageUri.get(i).getPath());
                RequestBody fileBody = RequestBody.create(mediaType, file);

                //Setting the file name as an empty string here causes the same issue, which is sending the request successfully without saving the files in the backend, so don't neglect the file name parameter.
                imageFileParts[i] = MultipartBody.Part.createFormData("complaint_image[]", file.getName(), fileBody);
            }
        }*/
        showProgress("Sending Estimations...");
        Observable<PostEstimationResponseDO> callEOs = mWebAPI.giveEstimation(requestDO);
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.newThread())
                .subscribe(x -> processEOs(x), x -> onError(x));
        disposables.add(callLogin);

    }

    private void processEOs(PostEstimationResponseDO loginResponse){
        hideProgress();
        getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
    }

    private void onError(Throwable throwable) {
//        Snackbar.make(etLogin, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    private void callSendEstimateImages(){

        MultipartBody.Part[] imageFileParts = null;
        MediaType mediaType = MediaType.parse("");
        if (imageUri.size() > 0) {
            imageFileParts = new MultipartBody.Part[imageUri.size()];
            for (int i = 0; i < imageUri.size(); i++) {
                File file = new File(imageUri.get(i).getPath());
                RequestBody fileBody = RequestBody.create(mediaType, file);
                //Setting the file name as an empty string here causes the same issue, which is sending the request successfully without saving the files in the backend, so don't neglect the file name parameter.
                imageFileParts[i] = MultipartBody.Part.createFormData("image[]", file.getName(), fileBody);
            }
        }

        Observable<EstimatedImageUploadRequestDO> callEOs = mWebAPI.postEstimatedImagesUpload(imageFileParts);
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processImages(x), x -> onError(x));
        disposables.add(callLogin);
    }

    private void processImages(EstimatedImageUploadRequestDO uploadRequestDO){
        globalUrls = uploadRequestDO.getImages();
        callWebForContent();
    }

}
