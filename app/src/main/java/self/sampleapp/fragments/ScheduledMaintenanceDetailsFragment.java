package self.sampleapp.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.sampleapp.App;
import self.sampleapp.R;
import self.sampleapp.adapters.AddComplaintImagesAdapter;
import self.sampleapp.model.AcceptRejectResponseDO;
import self.sampleapp.model.Schedule;
import self.sampleapp.web.WebAPI;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


/**
 * Created by PsSri on 08-07-2017.
 */

public class ScheduledMaintenanceDetailsFragment extends Fragment implements View.OnClickListener{
    /*@BindView(R.id.ivEstimationSelectImage)
    ImageView ivEstimationSelectImage;
    @BindView(R.id.ivSelectVideo)
    ImageView ivSelectVideo;
    *//*@BindView(R.id.btnSubmit)
    Button btnSubmit;*//*
    @BindView(R.id.videoPreview)
    VideoView videoPreview;
    @BindView(R.id.rvEstimationImages)
    RecyclerView rvEstimationImages;
    @BindView(R.id.flVideo)
    FrameLayout flVideo;
    @BindView(R.id.btnClose)
    ImageView btnClose;*/
//    @BindView(R.id.iv_tech_view_sign_super)
//    ImageView btnSignSupervisor;
//    @BindView(R.id.iv_tech_view_sign_tech)
//    ImageView btnSignTechnician;

    private EditText subjectEditText;
    private EditText departmentEditText;
    private EditText descriptionEditText;
    private EditText dateEditText;
    private EditText timeslotEditText;
    private TextView contactDetailsTextView;
    private ImageView nameCardImageView;
    private ImageView uploadedImagetumbnail1;
    private ImageView uploadedImagetumbnail2;
    private ImageView uploadedImagetumbnail3;
    private ImageView uploadedVideotumbnai11;


    private LinearLayout acceptRejectLayout;
    private LinearLayout giveEstimateLayout;
    private LinearLayout reestimateLayout;
    private LinearLayout signatureLayout;
    //Button in the above layouts
    private Button acceptButton;
    private Button rejectButton;
    private Button giveEstimateButton;
    private Button viewEstimationButton;
    private Button reEstimateButton;
    private Button workOrderButton;
    private Button viewEstimationWithSignatureButton;
    private Button workOrderWithSignatureButton;
    private Button closeComplaintButton;
    private String[] splitImageUrls;
    private String[] splitVideoUrls;
    //Web Service related
    @Inject
    SharedPreferences sPref;
    @Inject
    WebAPI mWebAPI;

    private AddComplaintImagesAdapter aCImages;

    private static final int SUPERVISOR_SIGNATURE_CAPTURE_REQUEST_CODE = 1111;
    private static final int TECHNICIAN_SIGNATURE_CAPTURE_REQUEST_CODE = 2222;
    private static final int SIGNATURE_RESULTCODE = 1666;
    // Camera activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "RisolveFileUpload";

    private Uri fileUri; // file url to store image/video
    private CompositeDisposable disposables = new CompositeDisposable();

    private Schedule scheduleObject;
    private int containerID=-1;
    public static ScheduledMaintenanceDetailsFragment newInstance() {
        ScheduledMaintenanceDetailsFragment fragment = new ScheduledMaintenanceDetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args!=null)
        {
            scheduleObject = (Schedule) args.getSerializable("Schedule");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        containerID = container.getId();
        View rootView = inflater.inflate(R.layout.tech_view_complaint_fragment, container, false);
        //ButterKnife.bind(getActivity());
        ButterKnife.bind(this, rootView);
        //((App) getActivity().getApplication()).getNetComponent().inject();
        ((App)getActivity().getApplication()).getNetComponent().inject(this);

        subjectEditText = (EditText) rootView.findViewById(R.id.et_tech_view_subject);
        departmentEditText = (EditText)rootView.findViewById(R.id.et_tech_view_department);
        descriptionEditText = (EditText)rootView.findViewById(R.id.et_tech_view_description);
        dateEditText = (EditText)rootView.findViewById(R.id.et_tech_view_date);
        timeslotEditText = (EditText)rootView.findViewById(R.id.et_tech_view_timeslot);
        contactDetailsTextView = (TextView) rootView.findViewById(R.id.tv_tech_view_contactdetails);
        nameCardImageView = (ImageView)rootView.findViewById(R.id.iv_tech_view_namecard);
        uploadedImagetumbnail1 = (ImageView) rootView.findViewById(R.id.iv_tech_view_uploaded_1);
        uploadedImagetumbnail1.setOnClickListener(this);
        uploadedImagetumbnail2 = (ImageView)rootView.findViewById(R.id.iv_tech_view_uploaded_2);
        uploadedImagetumbnail2.setOnClickListener(this);
        uploadedImagetumbnail3 = (ImageView)rootView.findViewById(R.id.iv_tech_view_uploaded_3);
        uploadedImagetumbnail3.setOnClickListener(this);
        uploadedVideotumbnai11 = (ImageView)rootView.findViewById(R.id.iv_tech_view_video);
        acceptButton = (Button) rootView.findViewById(R.id.bt_tech_view_accept);
        contactDetailsTextView.setOnClickListener(contactDetailsClickListener);

        // LinearLayout ll_closecomplaint = (LinearLayout)rootView.findViewById(R.id.bt_tech_view_close_complaint);
        // ll_closecomplaint.setVisibility(View.VISIBLE);
        //btnSignSupervisor.setOnClickListener(this);
        //tnSignTechnician.setOnClickListener(this);
        //setting Data
        subjectEditText.setText(scheduleObject.getBma_schedule_name());
        departmentEditText.setText(scheduleObject.getDepartment_name());
        descriptionEditText.setText(scheduleObject.getBma_targeted_date());
        dateEditText.setText(scheduleObject.getBma_targeted_date());
        // timeslotEditText.setText(scheduleObject.getComplaintAvailabulityTime2());


        //String ImageURLS = scheduleObject.getComplaintImages();
       /* if(ImageURLS !=null && !TextUtils.isEmpty(ImageURLS)) {
            splitImageUrls = ImageURLS.split(",");
            if (splitImageUrls.length >= 1)
                Picasso.with(getActivity())
                        .load(splitImageUrls[0]).resize(300,300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedImagetumbnail1);
            else
                uploadedImagetumbnail1.setVisibility(View.GONE);
            if (splitImageUrls.length >= 2)
                Picasso.with(getActivity())
                        .load(splitImageUrls[1]).resize(300,300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedImagetumbnail2);
            else
                uploadedImagetumbnail2.setVisibility(View.GONE);
            if (splitImageUrls.length >= 3)
                Picasso.with(getActivity())
                        .load(splitImageUrls[2]).resize(300,300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedImagetumbnail3);
            else
                uploadedImagetumbnail3.setVisibility(View.GONE);
        }*/

       /* String VideoURLS = scheduleObject.getComplaintVideos();
        if(VideoURLS !=null && !TextUtils.isEmpty(VideoURLS)) {
            splitVideoUrls = VideoURLS.split(",");
            if (splitVideoUrls.length >= 1) {
                    try {
                        Bitmap bmp = retriveVideoFrameFromVideo(splitVideoUrls[0]);
                        uploadedVideotumbnai11.setImageBitmap(bmp);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                }
            }*/
                /*Picasso.with(getActivity())
                        .load(splitImageUrls[0]).resize(300, 300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedVideotumbnai11);*/
        //layouts for button according to status of the complaint
        acceptRejectLayout = (LinearLayout) rootView.findViewById(R.id.accept_reject_layout);
        acceptButton = (Button)rootView.findViewById(R.id.bt_tech_view_accept);
        acceptButton.setOnClickListener(this);
        rejectButton = (Button)rootView.findViewById(R.id.bt_tech_view_reject);
        rejectButton.setOnClickListener(this);
        //give estimation layout
        giveEstimateLayout = (LinearLayout) rootView.findViewById(R.id.give_estimate_layout);
        giveEstimateButton = (Button)rootView.findViewById(R.id.bt_tech_view_give_estimate);
        giveEstimateButton.setOnClickListener(this);

        reestimateLayout = (LinearLayout) rootView.findViewById(R.id.re_estimate_layout);
        viewEstimationButton = (Button)rootView.findViewById(R.id.bt_tech_view_view_estimate);
        viewEstimationButton.setOnClickListener(this);
        reEstimateButton = (Button)rootView.findViewById(R.id.bt_tech_view_reestimate);
        reEstimateButton.setOnClickListener(this);
        workOrderButton = (Button)rootView.findViewById(R.id.bt_tech_view_workorder);
        workOrderButton.setOnClickListener(this);

        signatureLayout = (LinearLayout) rootView.findViewById(R.id.ll_ongoing_complaint);
        //  viewEstimationWithSignatureButton = (Button)rootView.findViewById(R.id.bt_tech_view_sign_view_estimate);
//        viewEstimationWithSignatureButton.setOnClickListener(this);
        workOrderWithSignatureButton = (Button)rootView.findViewById(R.id.bt_tech_view_ongoing_order);
        workOrderWithSignatureButton.setOnClickListener(this);
        closeComplaintButton = (Button)rootView.findViewById(R.id.bt_tech_view_ongoing_close_complaint);
        closeComplaintButton.setOnClickListener(this);

        signatureLayout.setVisibility(View.VISIBLE);
       /* if(scheduleObject.getComplaintStatus().equals("2"))//new complaint
        {
            acceptRejectLayout.setVisibility(View.VISIBLE);
        }else if(scheduleObject.getComplaintStatus().equals("3"))//accepted Complaint
        {
            giveEstimateLayout.setVisibility(View.VISIBLE);
        }
        else if(scheduleObject.getComplaintStatus().equals("4"))//Estimated Complaint
        {
            reestimateLayout.setVisibility(View.VISIBLE);
        }
        else if(scheduleObject.getComplaintStatus().equals("7"))//Estimated Complaint
        {
            signatureLayout.setVisibility(View.VISIBLE);
        }*/

       /* ivEstimationSelectImage.setOnClickListener((view) -> captureImage());
        ivSelectVideo.setOnClickListener((view) -> recordVideo());
        btnClose.setOnClickListener((view) -> removeVideo());

        LinearLayoutManager lmExOffers = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvEstimationImages.setLayoutManager(lmExOffers);
        aCImages = new AddComplaintImagesAdapter(null, rvEstimationImages, ivEstimationSelectImage);
        rvEstimationImages.setAdapter(aCImages);*/

        return rootView;
    }

    View.OnClickListener contactDetailsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           /*ContactDialogFragment contactDialogFragment = new ContactDialogFragment();
            contactDialogFragment.show(getFragmentManager(), "missiles");*/
            showAddressAlert();
        }
    };

    @Override
    public void onClick(View v) {
        Fragment selectedFragment =null;
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        switch (v.getId()){
            case R.id.bt_tech_view_accept:
                postAcceptanceOfComplaint(true);
                break;
            case R.id.accept_reject_layout:
                postAcceptanceOfComplaint(false);
                break;
            case R.id.bt_tech_view_give_estimate:
                selectedFragment = OrderEstimatesFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putBoolean("SHOW_ADD", true);
                selectedFragment.setArguments(bundle);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
            case R.id.bt_tech_view_view_estimate:
                selectedFragment = OrderEstimatesFragment.newInstance();
                Bundle bundle1 = new Bundle();
                bundle1.putBoolean("SHOW_ADD", false);
                selectedFragment.setArguments(bundle1);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
            case R.id.bt_tech_view_reestimate:
                selectedFragment = OrderEstimatesFragment.newInstance();
                Bundle bundle2 = new Bundle();
                bundle2.putBoolean("SHOW_ADD", true);
                selectedFragment.setArguments(bundle2);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
            case R.id.bt_tech_view_workorder:
                break;
           /* case R.id.bt_tech_view_sign_view_estimate:
                 selectedFragment = OrderEstimatesFragment.newInstance();
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
            case R.id.bt_tech_view_sign_order:
                //show pdf
                *//*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(scheduleObject));
                startActivity(browserIntent);*//*
                break;*/
            case R.id.bt_tech_view_ongoing_close_complaint:
                selectedFragment = SubmitServiceReportOrCloseFragment.newInstance();
                Bundle bundle4 = new Bundle();
                // bundle4.putString("COMPLAINT_ID", scheduleObject.getComplaintId());
                selectedFragment.setArguments(bundle4);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                /*Intent signIntent = new Intent(getActivity(), Signature.class);
                getActivity().startActivityForResult(signIntent,1234);*/
                break;
            case R.id.iv_tech_view_uploaded_1:
            case R.id.iv_tech_view_uploaded_2:
            case R.id.iv_tech_view_uploaded_3:
                selectedFragment = ImageViewPager.newInstance();
                Bundle bundle3 = new Bundle();
                bundle3.putString("TITLE", "User Uploaded");
                bundle3.putStringArray("IMAGE_URLS",splitImageUrls);
                selectedFragment.setArguments(bundle3);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
         /*   case R.id.iv_tech_view_sign_super:
                Intent signIntent1 = new Intent(getActivity(), Signature.class);
                startActivityForResult(signIntent1,SUPERVISOR_SIGNATURE_CAPTURE_REQUEST_CODE);
                break;
            case R.id.iv_tech_view_sign_tech:
                Intent signIntent2 = new Intent(getActivity(), Signature.class);
                startActivityForResult(signIntent2,TECHNICIAN_SIGNATURE_CAPTURE_REQUEST_CODE);
                break;*/

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("Schedule", "onActivityResult::: "+resultCode);
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // launching upload activity
                addMedia(1);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Snackbar.make(contactDetailsTextView, "User cancelled image capture", Snackbar.LENGTH_SHORT).show();
            } else {
                // failed to capture image
                Snackbar.make(contactDetailsTextView, "Sorry! Failed to capture image", Snackbar.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // video successfully recorded
                // launching upload activity
                addMedia(2);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled recording
                Snackbar.make(contactDetailsTextView, "User cancelled video recording", Snackbar.LENGTH_SHORT).show();
            } else {
                // failed to record video
                Snackbar.make(contactDetailsTextView, "Sorry! Failed to record video", Snackbar.LENGTH_SHORT).show();
            }

        } else if (requestCode == SUPERVISOR_SIGNATURE_CAPTURE_REQUEST_CODE) {
            if(resultCode == SIGNATURE_RESULTCODE)
            {
                byte[] byteArray = data.getByteArrayExtra("IMAGE_STRING");
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                //btnSignSupervisor.setImageBitmap(bmp);
            }
        }
        else if (requestCode == TECHNICIAN_SIGNATURE_CAPTURE_REQUEST_CODE) {
            if(resultCode == SIGNATURE_RESULTCODE)
            {
                byte[] byteArray = data.getByteArrayExtra("IMAGE_STRING");
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                //btnSignTechnician.setImageBitmap(bmp);
            }
        }
    }

    private Uri videoUri;
    private ArrayList<Uri> imageUri = new ArrayList<>();

    private void addMedia(int mediaType) {
        if (fileUri == null || TextUtils.isEmpty(fileUri.getPath()))
            return;

        Log.i("Uri", fileUri.getPath());
        /*if (mediaType == 2) {
            videoUri = fileUri;
            ivSelectVideo.setVisibility(View.GONE);
            flVideo.setVisibility(View.VISIBLE);
            videoPreview.setVideoPath(videoUri.getPath());
        } else {
            imageUri.add(fileUri);
            if (imageUri.size() == 3) {
                ivEstimationSelectImage.setVisibility(View.GONE);
            } else {
                ivEstimationSelectImage.setVisibility(View.VISIBLE);
            }
            aCImages.refresh(imageUri);
            rvEstimationImages.setVisibility(View.VISIBLE);
        }*/
    }

    private void removeImage(int position) {
        imageUri.remove(position);
    }

    private void removeVideo() {
        videoUri = null;
//        videoPreview.setVideoPath(null);
       /* flVideo.setVisibility(View.GONE);
        ivSelectVideo.setVisibility(View.VISIBLE);*/
    }

    /**
     * Launching camera app to capture image
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Launching camera app to record video
     */
    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("AddComplaintFragment", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
    private void postAcceptanceOfComplaint(boolean isAccepted){
        String projectId = sPref.getString("bmaProjectId", "");
        Map<String, String> names = generateMap(isAccepted);
        Observable<AcceptRejectResponseDO> callEOs = mWebAPI.acceptRejectComplaints(names);
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> onSuccess(response), x -> onError(x));
        disposables.add(callLogin);


    }
    private Map<String, String> generateMap(boolean isAccepted) {
        /*
        "accept_rejected=1
        user_id=5
        complaint_id=4
        accepted_time=7/7/2017 9.00 AM

        Required Fields:
        accept_rejected [1 is for accept and 2 is for rejected]--->mandatary
        user_id is the technician id --->mandatary
        complaint_id --->mandatary
        accepted_time --->optional
        "
         */
        String bmaUserId = sPref.getString("bmaUserId", "");
        Map<String, String> names = new HashMap<>();
        if(isAccepted)
            names.put("accept_rejected", "1");
        else
            names.put("accept_rejected", "2");

        names.put("user_id", bmaUserId);
        // names.put("complaint_id", scheduleObject.getComplaintId());
        return names;
    }
    private void onSuccess(AcceptRejectResponseDO responseDO){
        String responseMsg = responseDO.getMsg();
        Toast.makeText(getActivity(), ""+responseMsg, Toast.LENGTH_SHORT).show();
        getActivity().getSupportFragmentManager().popBackStack();
        // rvCommunityUpdates.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
        // tvNoCoUp.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
        // aExOffers.refresh(ads);
    }


    private void onError(Throwable throwable) {
        //Snackbar.make(tvComplaints, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    private void showAddressAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.user_contactdetails_custome_dialog,null);
        final EditText userNameEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_name);
        final EditText contactNoEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_mobile);
        final EditText addressEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_address);

        /*userNameEditText.setText(scheduleObject.getBmaUserName());
        contactNoEditText.setText(scheduleObject.getBmaUserMobile());
        addressEditText.setText(scheduleObject.getBmaUserBuildingId());*/

        builder.setView(dialog_layout)

                /*.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String quantity = Integer.parseInt(quantityEditText.getText().toString());
                        String itemname = itemNameEditText.getText().toString();
                        double unitprice = Double.parseDouble(unitPriceEditText.getText().toString());
                        double labourcharges = Double.parseDouble(labourChargesEditText.getText().toString());

                        EstimateMaterial item = new EstimateMaterial();
                        item.quantity = quantity;
                        item.itemName = itemname;
                        item.unitPrice = unitprice;
                        item.labourCharges = labourcharges;

                        items.add(item);
                        materialAdapter.notifyItemChanged(items.indexOf(item));
                        caluclateAndSetTotalCharges();
                        //materialAdapter.notifyDataSetChanged();

                    }
                })*/
                .setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    private long getVideoDuration() {
        if (videoUri == null)
            return 0;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(getActivity(), videoUri);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillis = Long.parseLong(time);
        retriever.release();
        return timeInMillis;
    }

    private long getVideoFileSize() {
        if (videoUri == null)
            return 0;
        return new File(videoUri.getPath()).length();
    }
}
