package self.sampleapp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import self.sampleapp.R;
import self.sampleapp.model.ComplaintDO;

/**
 * Created by Gopi on 11/16/2016.
 */

public class ReportsFragment extends Fragment {
    private TabLayout tlCompliantsTab;
    private ViewPager vpCompliant;

    private FloatingActionButton createComplaintFab, createSuggestionFab, createResquestFab, fab;
    private LinearLayout createComplaintLayout, createSuggestionLayout, createRequestLayout;
    private boolean isFabMenuOpen = false, isFirstDocUpload = false;
    private Toolbar toolbar;
    private Animation fabOpenAnimation, fabCloseAnimation;
//    private MaterialSearchView searchView;
    ArrayList<ComplaintDO> iTunesItemList;

    public static ReportsFragment newInstance() {
        ReportsFragment fragment = new ReportsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.compliants_container_fragment, container, false);

//        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        getAnimations();
        initializeControls(view);
        bindViewControls();

        return view;
    }

    public void setData(ArrayList<ComplaintDO> iTunesItemList)
    {
        this.iTunesItemList = iTunesItemList;
    }

   /* private void getAnimations() {

        fabOpenAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fabCloseAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);

    }*/

    private void initializeControls(View view) {
        tlCompliantsTab       = (TabLayout) view.findViewById(R.id.tlCompliantsTab);
        vpCompliant           = (ViewPager) view.findViewById(R.id.vpCompliant);

//        createResquestFab = (FloatingActionButton) view.findViewById(R.id.createResquestFab);
//        createSuggestionFab = (FloatingActionButton) view.findViewById(R.id.createSuggestionFab);
//        createComplaintFab = (FloatingActionButton) view.findViewById(R.id.createComplaintFab);
//        createComplaintLayout = (LinearLayout) view.findViewById(R.id.createComplaintLayout);
//        createSuggestionLayout = (LinearLayout) view.findViewById(R.id.createSuggestionLayout);
//        createRequestLayout = (LinearLayout) view.findViewById(R.id.createResquestLayout);

//        fab = (FloatingActionButton) view.findViewById(R.id.fab);
    }
    private void bindViewControls(){

        setupViewPager(vpCompliant);
        vpCompliant.setCurrentItem(0);
        tlCompliantsTab.setupWithViewPager(vpCompliant);
        tlCompliantsTab.setSelectedTabIndicatorColor(Color.parseColor("#284a5a"));
        tlCompliantsTab.setSelectedTabIndicatorHeight(10);
        tlCompliantsTab.setTabTextColors(Color.parseColor("#3a6b83"), Color.parseColor("#284a5a"));

        /*createRequestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRequestFragment(1);
            }
        });

        createSuggestionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRequestFragment(2);
            }
        });

        createComplaintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callRequestFragment(3);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isFabMenuOpen)
                    collapseFabMenu();
                else
                    expandFabMenu();
            }
        });*/
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new EstimatesFragment("ESTIMATES"), "ESTIMATES");
        adapter.addFragment(new WorkOrdersFragment("WORK ORDERS"), "WORK ORDERS");
        adapter.addFragment(new ServiceReportsFragment("SERVICE REPORTS"), "SERVICE REPORTS");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    /*private void callRequestFragment(int requestType) {
        collapseFabMenu();
        fab.setVisibility(View.INVISIBLE);
        AddRequestFragment addTicketFragment = new AddRequestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("requestType", requestType);
        addTicketFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.container, addTicketFragment).addToBackStack(null).commit();
    }

    private void expandFabMenu() {
        fab.animate().rotation(45.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        createComplaintLayout.startAnimation(fabOpenAnimation);
        createSuggestionLayout.startAnimation(fabOpenAnimation);
        createRequestLayout.startAnimation(fabOpenAnimation);
        isFabMenuOpen = true;
    }

    private void collapseFabMenu() {
        fab.animate().rotation(0.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        createComplaintLayout.startAnimation(fabCloseAnimation);
        createSuggestionLayout.startAnimation(fabCloseAnimation);
        createRequestLayout.startAnimation(fabCloseAnimation);
        isFabMenuOpen = false;
    }*/

}