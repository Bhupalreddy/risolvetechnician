package self.sampleapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import self.sampleapp.App;
import self.sampleapp.BaseFragment;
import self.sampleapp.R;
import self.sampleapp.Utils.MarshMallowPermission;
import self.sampleapp.Utils.RisolveConstants;
import self.sampleapp.Utils.RisolveLocation;
import self.sampleapp.adapters.AddComplaintImagesAdapter;
import self.sampleapp.fragments.signature.Signature;
import self.sampleapp.model.PostCloseComplaintResponse;
import self.sampleapp.web.WebAPI;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static okhttp3.RequestBody.create;


/**
 * Created by PsSri on 08-07-2017.
 */

public class SubmitServiceReportOrCloseFragment extends BaseFragment implements View.OnClickListener{

    @BindView(R.id.iv_tech_close_sign_super)
    ImageView btnSignUser;
    @BindView(R.id.iv_tech_close_sign_tech)
    ImageView btnSignTechnician;
    @BindView(R.id.tx_supervisor_sign)
    TextView supervisorSignTextView;
    @BindView(R.id.tx_technician_sign)
    TextView tx_technician_sign;
    @BindView(R.id.etTechnicianDesc)
    EditText etTechnicianDesc;

    @BindView(R.id.ivSelectImage)
    ImageView ivSelectImage;

    @BindView(R.id.rvImages)
    RecyclerView rvImages;
    @BindView(R.id.bt_tech_close_close_complaint)
    Button btnCloseComplaint;
    @BindView(R.id.iv_close_upload1) ImageView uploadCloseImageView1;
    private AddComplaintImagesAdapter aCImages;

    private static final int SUPERVISOR_SIGNATURE_CAPTURE_REQUEST_CODE = 1111;
    private static final int TECHNICIAN_SIGNATURE_CAPTURE_REQUEST_CODE = 2222;
    private static final int SIGNATURE_RESULTCODE = 1666;
    // Camera activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "RisolveFileUpload";

    private int REQUEST_CAMERA = 2;
    String mCurrentPhotoPath;
    private int screenWidth, screenHeight;

    private Uri fileUri; // file url to store image/video
    private MarshMallowPermission marshMallowPermission;

    //Wen Service related
    @Inject
    SharedPreferences sPref;
    @Inject
    WebAPI mWebAPI;
    private CompositeDisposable disposables = new CompositeDisposable();
    private static Context mContext;

    //private Complaint complaintObject;
    private String mComplaintId;
    private int containerID=-1;
    public static SubmitServiceReportOrCloseFragment newInstance() {
        SubmitServiceReportOrCloseFragment fragment = new SubmitServiceReportOrCloseFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mContext = getActivity();
        if(args!=null)
        {
            mComplaintId =  args.getString("COMPLAINT_ID");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        containerID = container.getId();
        View rootView = inflater.inflate(R.layout.submit_service_report_or_close_complaint_fragment, container, false);
        ButterKnife.bind(this,rootView);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
        marshMallowPermission = new MarshMallowPermission(getActivity());
        setUpDisplayScreenResolution(getActivity());
        btnSignUser.setOnClickListener(this);
        btnSignTechnician.setOnClickListener(this);
        btnCloseComplaint.setOnClickListener(this);
        ivSelectImage.setOnClickListener((view) -> checkRuntimeCameraPermissionsAndProceed());
        // btnClose.setOnClickListener((view) -> removeVideo());

        LinearLayoutManager lmExOffers = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(lmExOffers);
        aCImages = new AddComplaintImagesAdapter(null, rvImages, ivSelectImage);
        rvImages.setAdapter(aCImages);
        //Calling to get gps location and settings
        new RisolveLocation().getLocation(getActivity());
        return rootView;
    }

    @Override
    public void onClick(View v) {
        Fragment selectedFragment =null;
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        switch (v.getId()){

           /* case R.id.iv_tech_close_sign_super:
                Intent signIntent = new Intent(getActivity(), Signature.class);
                getActivity().startActivityForResult(signIntent,1234);
                break;*/
            case R.id.iv_tech_close_sign_super:
            case R.id.tx_supervisor_sign:
                Intent signIntent1 = new Intent(getActivity(), Signature.class);
                startActivityForResult(signIntent1,SUPERVISOR_SIGNATURE_CAPTURE_REQUEST_CODE);
                break;
            case R.id.iv_tech_close_sign_tech:
            case R.id.tx_technician_sign:
                Intent signIntent2 = new Intent(getActivity(), Signature.class);
                startActivityForResult(signIntent2,TECHNICIAN_SIGNATURE_CAPTURE_REQUEST_CODE);
                break;
            case R.id.bt_tech_close_close_complaint:
                //call web service to close the complaint
                submitComplaint();
                break;
        }
    }

    //=====================================================Close Complaint Web Service call
    private void submitComplaint() {

        String technicianDesc = etTechnicianDesc.getText().toString().trim();
        String compaintStatus = RisolveConstants.COMPLAINT_STATUS_CODE_CLOSED;
        String message = null;
        if(TextUtils.isEmpty(etTechnicianDesc.getText().toString().trim())){
            message = "Technician description shouldn't be empty.";
        }
        else if(TextUtils.isEmpty(mComplaintId)){
            message = "Complaint Id shouldn't be empty.";
        }


        if(message != null){
            Snackbar.make(supervisorSignTextView, message, Snackbar.LENGTH_SHORT).show();
            return;
        }

        showProgress("Closing Complaint...");
        /*
        "complaint_id=1
        technician_review=this problem is solved
        technician_uplode_images[]=images.jpg
        user_signature=siva.jpg
        complaint_status=7 [7 for closed]
        tech_signature=sivasignature.jpg"
         */
        MediaType mediaType = MediaType.parse("");
        RequestBody complaintSubject = create(mediaType, technicianDesc);
        RequestBody complaintDesc = create(mediaType, compaintStatus);
        RequestBody complaintId = create(mediaType, mComplaintId);

        MultipartBody.Part[] imageFileParts = null;
        if (imageUri.size() > 0) {
            imageFileParts = new MultipartBody.Part[imageUri.size()];
            for (int i = 0; i < imageUri.size(); i++) {
                File file = new File(imageUri.get(i).getPath());
                RequestBody fileBody = RequestBody.create(mediaType, file);
                //Setting the file name as an empty string here causes the same issue, which is sending the request successfully without saving the files in the backend, so don't neglect the file name parameter.
                imageFileParts[i] = MultipartBody.Part.createFormData("technician_uplode_images[]", file.getName(), fileBody);
            }
        }

        MultipartBody.Part userSignatureImage = null;
        File file = getFileFromBitmap(((BitmapDrawable)btnSignUser.getDrawable()).getBitmap(),"usersignature.jpg");
        RequestBody fileBody = RequestBody.create(mediaType, file);
        userSignatureImage = MultipartBody.Part.createFormData("user_signature", file.getName(), fileBody);

        MultipartBody.Part technicianSignatureImage = null;
        File file2 = getFileFromBitmap(((BitmapDrawable)btnSignTechnician.getDrawable()).getBitmap(),"techsignature.jpg");
        RequestBody fileBody2 = RequestBody.create(mediaType, file2);
        technicianSignatureImage = MultipartBody.Part.createFormData("tech_signature", file2.getName(), fileBody2);

        Observable<PostCloseComplaintResponse> call = mWebAPI.postCloseComplaint(complaintId,complaintSubject, complaintDesc, imageFileParts,userSignatureImage,technicianSignatureImage);
        Disposable callComplaints = call
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pcr -> processComplaintSubmission(pcr),
                        throwable -> onCloseError(throwable));
        disposables.add(callComplaints);
    }

    private void processComplaintSubmission(PostCloseComplaintResponse postCloseComplaintResponse) {
        Log.i("PostComplaintResponse", postCloseComplaintResponse.getMsg());
        if (getActivity() == null)
            return;
        hideProgress();
        Snackbar.make(supervisorSignTextView, postCloseComplaintResponse.getMsg(), Snackbar.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    private void onCloseError(Throwable throwable) {
        hideProgress();
        Snackbar.make(supervisorSignTextView, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

//    private Disposable callComplaints;
    //================================================================================================




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("Schedule", "onActivityResult::: "+resultCode);
        // if the result is capturing Image
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // launching upload activity
//                addMedia(1);
                Bitmap originalImg = null;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                originalImg = setPic();
                if (originalImg != null) {
                    galleryAddPic();

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                    originalImg.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    bytes1 = bytes;
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".png");
                    Uri uri = Uri.fromFile(destination);
                    imageUri.add(uri);
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (imageUri.size() == 2) {
                        ivSelectImage.setVisibility(View.GONE);
                    } else {
                        ivSelectImage.setVisibility(View.VISIBLE);
                    }
                    ivSelectImage.setVisibility(View.VISIBLE);
                    aCImages.refresh(imageUri);
                } else if (resultCode == RESULT_CANCELED) {
                    // user cancelled Image capture
//                        Snackbar.make(new TextView(getActivity()), "User cancelled image capture", Snackbar.LENGTH_SHORT).show();
                } else {
                    // failed to capture image
//                        Snackbar.make(new TextView(getActivity()), "Sorry! Failed to capture image", Snackbar.LENGTH_SHORT).show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Snackbar.make(supervisorSignTextView, "User cancelled image capture", Snackbar.LENGTH_SHORT).show();
            } else {
                // failed to capture image
                Snackbar.make(supervisorSignTextView, "Sorry! Failed to capture image", Snackbar.LENGTH_SHORT).show();
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // video successfully recorded
                // launching upload activity
                addMedia(2);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled recording
                Snackbar.make(supervisorSignTextView, "User cancelled video recording", Snackbar.LENGTH_SHORT).show();
            } else {
                // failed to record video
                Snackbar.make(supervisorSignTextView, "Sorry! Failed to record video", Snackbar.LENGTH_SHORT).show();
            }

        } else if (requestCode == SUPERVISOR_SIGNATURE_CAPTURE_REQUEST_CODE) {
            if(resultCode == SIGNATURE_RESULTCODE)
            {
                byte[] byteArray = data.getByteArrayExtra("IMAGE_STRING");
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                btnSignUser.setImageBitmap(bmp);
            }
        }
        else if (requestCode == TECHNICIAN_SIGNATURE_CAPTURE_REQUEST_CODE) {
            if(resultCode == SIGNATURE_RESULTCODE)
            {
                byte[] byteArray = data.getByteArrayExtra("IMAGE_STRING");
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                btnSignTechnician.setImageBitmap(bmp);
            }
        }
    }

    private Uri videoUri;
    private ArrayList<Uri> imageUri = new ArrayList<>();

    private void addMedia(int mediaType) {
        if (fileUri == null || TextUtils.isEmpty(fileUri.getPath()))
            return;

        Log.i("Uri", fileUri.getPath());
        /*if (mediaType == 2) {
            videoUri = fileUri;
            ivSelectVideo.setVisibility(View.GONE);
            flVideo.setVisibility(View.VISIBLE);
            videoPreview.setVideoPath(videoUri.getPath());
        } else */{
            imageUri.add(fileUri);
            if (imageUri.size() == 3) {
                ivSelectImage.setVisibility(View.GONE);
            } else {
                ivSelectImage.setVisibility(View.VISIBLE);
            }
            aCImages.refresh(imageUri);
            rvImages.setVisibility(View.VISIBLE);
        }
    }

    private void removeImage(int position) {
        imageUri.remove(position);
    }

    /**
     * Launching camera app to capture image
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
//        File mediaStorageDir = new File(
//                Environment
//                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                IMAGE_DIRECTORY_NAME);

        File mediaStorageDir = new File(mContext.getCacheDir()+File.separator+IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("AddComplaintFragment", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    private File getFileFromBitmap(Bitmap bitmap, String filename)
    {
        //create a file to write bitmap data
        File f = new File(getActivity().getCacheDir(), filename);
        try {
            f.createNewFile();

            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        }catch (IOException e){}
        return f;
    }

    private void checkRuntimeCameraPermissionsAndProceed(){
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M)
        {
            boolean result =  marshMallowPermission.checkPermissionForCamera();
            if(result){
                boolean result2 =  marshMallowPermission.checkPermissionForExternalStorage();
                if(result2)
                {
                    dispatchTakePictureIntent();
                }
                else
                {
                    marshMallowPermission.requestPermissionForExternalStorage();
                }

            }
            else{
                marshMallowPermission.requestPermissionForCamera();
            }
        }
        else
        {
            dispatchTakePictureIntent();
        }
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),"com.example.self.sampleapp.fileprovider",photoFile);

                List<ResolveInfo> resolvedIntentActivities = getActivity().getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }


                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    //add photo to gallery
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private Bitmap setPic() {
        // Get the dimensions of the View
        int targetW = uploadCloseImageView1.getWidth();
        int targetH = uploadCloseImageView1.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        uploadCloseImageView1.setImageBitmap(bitmap);
        Log.d(mCurrentPhotoPath,"Resolution : "+photoW+"x"+photoH);
        return bitmap;
    }

    public void setUpDisplayScreenResolution(Context context) {
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        screenWidth = display.widthPixels;
        screenHeight = display.heightPixels;
    }
}
