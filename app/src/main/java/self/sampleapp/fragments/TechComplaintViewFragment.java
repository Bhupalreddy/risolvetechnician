package self.sampleapp.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.sampleapp.App;
import self.sampleapp.R;
import self.sampleapp.Utils.RisolveConstants;
import self.sampleapp.model.AcceptRejectResponseDO;
import self.sampleapp.model.Complaint;
import self.sampleapp.web.WebAPI;


/**
 * Created by PsSri on 08-07-2017.
 */

public class TechComplaintViewFragment extends Fragment implements View.OnClickListener{

    private EditText subjectEditText;
    private EditText departmentEditText;
    private EditText descriptionEditText;
    private EditText dateEditText;
    private EditText timeslotEditText;
    private TextView contactDetailsTextView;
    private ImageView nameCardImageView;
    private ImageView uploadedImagetumbnail1;
    private ImageView uploadedImagetumbnail2;
    private ImageView uploadedImagetumbnail3;
    private ImageView uploadedVideotumbnai11;

    private LinearLayout acceptRejectLayout;
    private LinearLayout giveEstimateLayout;
    private LinearLayout reestimateLayout;
    private LinearLayout submitReportOrCloseLayout;
    //Button in the above layouts
    private Button acceptButton;
    private Button rejectButton;
    private Button giveEstimateButton;
    private Button viewEstimationButton;
    private Button reEstimateButton;
    private Button workOrderButton;
    private Button viewEstimationWithSignatureButton;
    private Button workOrderInOnGoingButton;
    private Button closeInOnGoingButton;
    private String[] splitImageUrls;
    private String[] splitVideoUrls;
    //Wen Service related
    @Inject
    SharedPreferences sPref;
    @Inject
    WebAPI mWebAPI;
    private CompositeDisposable disposables = new CompositeDisposable();

    private Complaint complaintObject;
    private int containerID=-1;
    public static TechComplaintViewFragment newInstance() {
        TechComplaintViewFragment fragment = new TechComplaintViewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args!=null)
        {
            complaintObject = (Complaint) args.getSerializable("Complaint");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        containerID = container.getId();
        View rootView = inflater.inflate(R.layout.tech_view_complaint_fragment, container, false);
        ButterKnife.bind(getActivity(),rootView);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);

        subjectEditText = (EditText) rootView.findViewById(R.id.et_tech_view_subject);
        departmentEditText = (EditText)rootView.findViewById(R.id.et_tech_view_department);
        descriptionEditText = (EditText)rootView.findViewById(R.id.et_tech_view_description);
        dateEditText = (EditText)rootView.findViewById(R.id.et_tech_view_date);
        timeslotEditText = (EditText)rootView.findViewById(R.id.et_tech_view_timeslot);
        contactDetailsTextView = (TextView) rootView.findViewById(R.id.tv_tech_view_contactdetails);
        nameCardImageView = (ImageView)rootView.findViewById(R.id.iv_tech_view_namecard);
        uploadedImagetumbnail1 = (ImageView) rootView.findViewById(R.id.iv_tech_view_uploaded_1);
        uploadedImagetumbnail1.setOnClickListener(this);
        uploadedImagetumbnail2 = (ImageView)rootView.findViewById(R.id.iv_tech_view_uploaded_2);
        uploadedImagetumbnail2.setOnClickListener(this);
        uploadedImagetumbnail3 = (ImageView)rootView.findViewById(R.id.iv_tech_view_uploaded_3);
        uploadedImagetumbnail3.setOnClickListener(this);
        uploadedVideotumbnai11 = (ImageView)rootView.findViewById(R.id.iv_tech_view_video);
        uploadedVideotumbnai11.setOnClickListener(this);
        acceptButton = (Button) rootView.findViewById(R.id.bt_tech_view_accept);
        contactDetailsTextView.setOnClickListener(contactDetailsClickListener);
        //setting Data
        subjectEditText.setText(complaintObject.getComplaintSubject());
        departmentEditText.setText(complaintObject.getTechnicianDepartmentName());
        descriptionEditText.setText(complaintObject.getComplaintDescription());
        dateEditText.setText(complaintObject.getComplaintAvailabulityTime1());
        timeslotEditText.setText(complaintObject.getComplaintAvailabulityTime2());


        String ImageURLS = complaintObject.getComplaintImages();
        if(ImageURLS !=null && !TextUtils.isEmpty(ImageURLS)) {
            splitImageUrls = ImageURLS.split(",");
            if (splitImageUrls.length >= 1)
                Picasso.with(getActivity())
                        .load(splitImageUrls[0]).resize(300,300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedImagetumbnail1);
            else
                uploadedImagetumbnail1.setVisibility(View.GONE);
            if (splitImageUrls.length >= 2)
                Picasso.with(getActivity())
                        .load(splitImageUrls[1]).resize(300,300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedImagetumbnail2);
            else
                uploadedImagetumbnail2.setVisibility(View.GONE);
            if (splitImageUrls.length >= 3)
                Picasso.with(getActivity())
                        .load(splitImageUrls[2]).resize(300,300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedImagetumbnail3);
            else
                uploadedImagetumbnail3.setVisibility(View.GONE);
        }

        String VideoURLS = complaintObject.getComplaintVideos();
        if(VideoURLS !=null && !TextUtils.isEmpty(VideoURLS)) {
            splitVideoUrls = VideoURLS.split(",");
            if (splitVideoUrls.length >= 1) {
                    try {
                        Bitmap bmp = retriveVideoFrameFromVideo(splitVideoUrls[0]);
                        uploadedVideotumbnai11.setTag(splitVideoUrls[0]);
                        uploadedVideotumbnai11.setImageBitmap(bmp);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                }
            }
                /*Picasso.with(getActivity())
                        .load(splitImageUrls[0]).resize(300, 300)
                        .placeholder(R.drawable.progress_animation)
                        .into(uploadedVideotumbnai11);*/
        //layouts for button according to status of the complaint
        acceptRejectLayout = (LinearLayout) rootView.findViewById(R.id.accept_reject_layout);
        acceptButton = (Button)rootView.findViewById(R.id.bt_tech_view_accept);
        acceptButton.setOnClickListener(this);
        rejectButton = (Button)rootView.findViewById(R.id.bt_tech_view_reject);
        rejectButton.setOnClickListener(this);
        //give estimation layout
        giveEstimateLayout = (LinearLayout) rootView.findViewById(R.id.give_estimate_layout);
        giveEstimateButton = (Button)rootView.findViewById(R.id.bt_tech_view_give_estimate);
        giveEstimateButton.setOnClickListener(this);

        reestimateLayout = (LinearLayout) rootView.findViewById(R.id.re_estimate_layout);
        viewEstimationButton = (Button)rootView.findViewById(R.id.bt_tech_view_view_estimate);
        viewEstimationButton.setOnClickListener(this);
        reEstimateButton = (Button)rootView.findViewById(R.id.bt_tech_view_reestimate);
        reEstimateButton.setOnClickListener(this);
        workOrderButton = (Button)rootView.findViewById(R.id.bt_tech_view_workorder);
        workOrderButton.setOnClickListener(this);

        submitReportOrCloseLayout = (LinearLayout) rootView.findViewById(R.id.ll_ongoing_complaint);
        workOrderInOnGoingButton = (Button)rootView.findViewById(R.id.bt_tech_view_ongoing_order);
        workOrderInOnGoingButton.setOnClickListener(this);
        closeInOnGoingButton = (Button)rootView.findViewById(R.id.bt_tech_view_ongoing_close_complaint);
        closeInOnGoingButton.setOnClickListener(this);

//        complaintObject.setComplaintStatus("4"); //To Check the respective layouts

        if(complaintObject.getComplaintStatus().equals(RisolveConstants.COMPLAINT_STATUS_CODE_NEW))//new complaint
        {
            acceptRejectLayout.setVisibility(View.VISIBLE);
        }else if(complaintObject.getComplaintStatus().equals(RisolveConstants.COMPLAINT_STATUS_CODE_ASSIGN))//assigned Complaint
        {
            giveEstimateLayout.setVisibility(View.VISIBLE);
        }
        else if(complaintObject.getComplaintStatus().equals(RisolveConstants.COMPLAINT_STATUS_CODE_ESTIMATED))//Estimated Complaint
        {
            reestimateLayout.setVisibility(View.VISIBLE);
        }
        else if(complaintObject.getComplaintStatus().equals(RisolveConstants.COMPLAINT_STATUS_CODE_ONGOING))//ongoing Complaint
        {
            submitReportOrCloseLayout.setVisibility(View.VISIBLE);
        }
        return rootView;
    }

    View.OnClickListener contactDetailsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showAddressAlert();
        }
    };

    @Override
    public void onClick(View v) {
        Fragment selectedFragment =null;
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        switch (v.getId()){
            case R.id.bt_tech_view_accept:
                    postAcceptanceOfComplaint(true);
                break;
            case R.id.accept_reject_layout:
                postAcceptanceOfComplaint(false);
                break;
            case R.id.bt_tech_view_give_estimate:
                selectedFragment = OrderEstimatesFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putBoolean("SHOW_ADD", true);
                bundle.putString("complaintID", complaintObject.getComplaintId());
                selectedFragment.setArguments(bundle);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
            case R.id.bt_tech_view_view_estimate:
                //show pdf
                if(!TextUtils.isEmpty(complaintObject.getEstimation_pdf())) {
                    Intent browserEstimationPdfIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(complaintObject.getEstimation_pdf()));
                    startActivity(browserEstimationPdfIntent);
                }
                break;
            case R.id.bt_tech_view_reestimate:
                selectedFragment = OrderEstimatesFragment.newInstance();
                Bundle bundle2 = new Bundle();
                bundle2.putBoolean("SHOW_ADD", true);
                selectedFragment.setArguments(bundle2);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
            case R.id.bt_tech_view_workorder:
                //show pdf
                if(!TextUtils.isEmpty(complaintObject.getWorkorder_pdf())) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(complaintObject.getWorkorder_pdf()));
                    startActivity(browserIntent);
                }
                break;
            /*case R.id.bt_tech_view_sign_view_estimate:
                 selectedFragment = OrderEstimatesFragment.newInstance();
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;*/
            case R.id.bt_tech_view_ongoing_order:
                //show pdf
                if(!TextUtils.isEmpty(complaintObject.getEstimation_pdf())) {
                    Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(complaintObject.getEstimation_pdf()));
                    startActivity(browserIntent1);
                }
                break;
            case R.id.bt_tech_view_ongoing_close_complaint:
                selectedFragment = SubmitServiceReportOrCloseFragment.newInstance();
                Bundle bundle4 = new Bundle();
                bundle4.putString("COMPLAINT_ID", complaintObject.getComplaintId());
                selectedFragment.setArguments(bundle4);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                /*Intent signIntent = new Intent(getActivity(), Signature.class);
                getActivity().startActivityForResult(signIntent,1234);*/
                break;
            case R.id.iv_tech_view_uploaded_1:
            case R.id.iv_tech_view_uploaded_2:
            case R.id.iv_tech_view_uploaded_3:
                selectedFragment = ImageViewPager.newInstance();
                Bundle bundle3 = new Bundle();
                bundle3.putString("TITLE", "User Uploaded");
                bundle3.putStringArray("IMAGE_URLS",splitImageUrls);
                selectedFragment.setArguments(bundle3);
                transaction.replace(containerID, selectedFragment).addToBackStack(null);
                transaction.commit();
                break;
            case R.id.iv_tech_view_video:
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setType("video/*");
                String videoUrl = (String) uploadedVideotumbnai11.getTag();
                if(!TextUtils.isEmpty(videoUrl)) {
                    intent.setData(Uri.parse(videoUrl));
                    startActivity(Intent.createChooser(intent, "Complete action using"));
                }
                break;
        }
    }

    private void postAcceptanceOfComplaint(boolean isAccepted){
        String projectId = sPref.getString("bmaProjectId", "");
        Map<String, String> names = generateMap(isAccepted);
        Observable<AcceptRejectResponseDO> callEOs = mWebAPI.acceptRejectComplaints(names);
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> onSuccess(response), x -> onError(x));
        disposables.add(callLogin);


    }
    private Map<String, String> generateMap(boolean isAccepted) {
        /*
        "accept_rejected=1
        user_id=5
        complaint_id=4
        accepted_time=7/7/2017 9.00 AM

        Required Fields:
        accept_rejected [1 is for accept and 2 is for rejected]--->mandatary
        user_id is the technician id --->mandatary
        complaint_id --->mandatary
        accepted_time --->optional
        "
         */
        String bmaUserId = sPref.getString("bmaUserId", "");
        Map<String, String> names = new HashMap<>();
        if(isAccepted)
            names.put("accept_rejected", "1");
        else
            names.put("accept_rejected", "2");

        names.put("user_id", bmaUserId);
        names.put("complaint_id", complaintObject.getComplaintId());
        return names;
    }
    private void onSuccess(AcceptRejectResponseDO responseDO){
        String responseMsg = responseDO.getMsg();
        Toast.makeText(getActivity(), ""+responseMsg, Toast.LENGTH_SHORT).show();
        getActivity().getSupportFragmentManager().popBackStack();
       // rvCommunityUpdates.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
       // tvNoCoUp.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
       // aExOffers.refresh(ads);
    }


    private void onError(Throwable throwable) {
        //Snackbar.make(tvComplaints, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    private void showAddressAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.user_contactdetails_custome_dialog,null);
        final EditText userNameEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_name);
        final EditText contactNoEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_mobile);
        final EditText addressEditText = (EditText) dialog_layout.findViewById(R.id.et_tech_view_res_address);

        userNameEditText.setText(complaintObject.getUserName());
        contactNoEditText.setText(complaintObject.getUserMobile());
        addressEditText.setText(complaintObject.getUserAddress());

        builder.setView(dialog_layout)

                /*.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String quantity = Integer.parseInt(quantityEditText.getText().toString());
                        String itemname = itemNameEditText.getText().toString();
                        double unitprice = Double.parseDouble(unitPriceEditText.getText().toString());
                        double labourcharges = Double.parseDouble(labourChargesEditText.getText().toString());

                        EstimateMaterial item = new EstimateMaterial();
                        item.quantity = quantity;
                        item.itemName = itemname;
                        item.unitPrice = unitprice;
                        item.labourCharges = labourcharges;

                        items.add(item);
                        materialAdapter.notifyItemChanged(items.indexOf(item));
                        caluclateAndSetTotalCharges();
                        //materialAdapter.notifyDataSetChanged();

                    }
                })*/
                .setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
}
