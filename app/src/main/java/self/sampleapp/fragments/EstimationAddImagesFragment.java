package self.sampleapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import self.sampleapp.App;
import self.sampleapp.R;
import self.sampleapp.Utils.MarshMallowPermission;
import self.sampleapp.adapters.AddComplaintImagesAdapter;
import self.sampleapp.model.Complaint;
import self.sampleapp.web.WebAPI;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static self.sampleapp.fragments.OrderEstimatesFragment.imageUri;


/**
 * Created by PsSri on 08-07-2017.
 */

public class EstimationAddImagesFragment extends Fragment implements View.OnClickListener{

    @BindView(R.id.ivEstimationSelectImage)
    ImageView ivEstimationSelectImage;

    @BindView(R.id.rvEstimationImages)
    RecyclerView rvEstimationImages;
    @BindView(R.id.bt_Estimation_close)
    Button btnCloseComplaint;
    private AddComplaintImagesAdapter aCImages;

    // Camera activity request codes
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;

    public static final int MEDIA_TYPE_IMAGE = 1;

    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "RisolveFileUpload";
    private int REQUEST_CAMERA = 2;
    String mCurrentPhotoPath;
    private int screenWidth, screenHeight;

    private Uri fileUri; // file url to store image/video
    private MarshMallowPermission marshMallowPermission;

    //Wen Service related
    @Inject
    SharedPreferences sPref;
    @Inject
    WebAPI mWebAPI;
    private CompositeDisposable disposables = new CompositeDisposable();

    private Complaint complaintObject;
    private int containerID=-1;
    public static EstimationAddImagesFragment newInstance() {
        EstimationAddImagesFragment fragment = new EstimationAddImagesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        /*if(args!=null)
        {
            complaintObject = (Complaint) args.getSerializable("Complaint");
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        containerID = container.getId();
        View rootView = inflater.inflate(R.layout.estimation_add_images_fragment, container, false);
        ButterKnife.bind(this,rootView);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
        marshMallowPermission = new MarshMallowPermission(getActivity());
        setUpDisplayScreenResolution(getActivity());
        ivEstimationSelectImage.setOnClickListener((view) -> checkRuntimeCameraPermissionsAndProceed());
       // btnClose.setOnClickListener((view) -> removeVideo());
        btnCloseComplaint.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvEstimationImages.setLayoutManager(linearLayoutManager);
        setLayoutParams(rvEstimationImages, R.drawable.ic_camera);
        aCImages = new AddComplaintImagesAdapter(null, rvEstimationImages, ivEstimationSelectImage);
        rvEstimationImages.setAdapter(aCImages);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        Fragment selectedFragment =null;
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        switch (v.getId()){
            case R.id.bt_Estimation_close:
                //call web service to close the complaint
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("Schedule", "onActivityResult::: "+resultCode);
        // if the result is capturing Image
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // launching upload activity
//                addMedia(1);
                Bitmap originalImg = null;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                originalImg = setPic();
                if (originalImg != null) {
                    galleryAddPic();

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    ByteArrayOutputStream bytes1 = new ByteArrayOutputStream();
                    originalImg.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    bytes1 = bytes;
                    File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".png");
                    Uri uri = Uri.fromFile(destination);
                    imageUri.add(uri);
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (imageUri.size() == 2) {
                        ivEstimationSelectImage.setVisibility(View.GONE);
                    } else {
                        ivEstimationSelectImage.setVisibility(View.VISIBLE);
                    }
                    aCImages.refresh(imageUri);
                    rvEstimationImages.setVisibility(View.VISIBLE);
                } else if (resultCode == RESULT_CANCELED) {
                    // user cancelled Image capture
                    Snackbar.make(new TextView(getActivity()), "User cancelled image capture", Snackbar.LENGTH_SHORT).show();
                } else {
                    // failed to capture image
                    Snackbar.make(new TextView(getActivity()), "Sorry! Failed to capture image", Snackbar.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // video successfully recorded
                // launching upload activity
                addMedia(2);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled recording
                Snackbar.make(new TextView(getActivity()), "User cancelled video recording", Snackbar.LENGTH_SHORT).show();
            } else {
                // failed to record video
                Snackbar.make(new TextView(getActivity()), "Sorry! Failed to record video", Snackbar.LENGTH_SHORT).show();
            }

        }
    }

//    private ArrayList<Uri> imageUri = new ArrayList<>();

    private void addMedia(int mediaType) {
        if (fileUri == null || TextUtils.isEmpty(fileUri.getPath()))
            return;

        Log.i("Uri", fileUri.getPath());
        /*if (mediaType == 2) {
            videoUri = fileUri;
            ivSelectVideo.setVisibility(View.GONE);
            flVideo.setVisibility(View.VISIBLE);
            videoPreview.setVideoPath(videoUri.getPath());
        } else */{
            imageUri.add(fileUri);
            if (imageUri.size() == 2) {
                ivEstimationSelectImage.setVisibility(View.GONE);
            } else {
                ivEstimationSelectImage.setVisibility(View.VISIBLE);
            }
            aCImages.refresh(imageUri);
            rvEstimationImages.setVisibility(View.VISIBLE);
        }
    }

    private void removeImage(int position) {
        imageUri.remove(position);
    }


    /**
     * Launching camera app to capture image
     */
    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }



    /**
     * Creating file uri to store image/video
     */
    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("AddComplaintFragment", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        }  else {
            return null;
        }

        return mediaFile;
    }


    private void checkRuntimeCameraPermissionsAndProceed(){
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M)
        {
            boolean result =  marshMallowPermission.checkPermissionForCamera();
            if(result){
                boolean result2 =  marshMallowPermission.checkPermissionForExternalStorage();
                if(result2)
                {
                    dispatchTakePictureIntent();
                }
                else
                {
                    marshMallowPermission.requestPermissionForExternalStorage();
                }

            }
            else{
                marshMallowPermission.requestPermissionForCamera();
            }
        }
        else
        {
            dispatchTakePictureIntent();
        }
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),"com.example.self.sampleapp.fileprovider",photoFile);

                List<ResolveInfo> resolvedIntentActivities = getActivity().getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }


                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CAMERA);
            }
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    //add photo to gallery
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private Bitmap setPic() {
        // Get the dimensions of the View
        /*int targetW = *//*imageView.getWidth()*//*500;
        int targetH = *//*imageView.getHeight()*//*500;*/

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/screenWidth, photoH/screenHeight);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        Log.d(mCurrentPhotoPath,"Resolution : "+photoW+"x"+photoH);
        return bitmap;
    }

    public void setUpDisplayScreenResolution(Context context) {
        DisplayMetrics display = context.getResources().getDisplayMetrics();
        screenWidth = display.widthPixels;
        screenHeight = display.heightPixels;
    }

    public void setLayoutParams(View v, int drawableId)
    {
        int [] dDimens = getDrawableDimensions(v.getContext(), drawableId);
//        v.getLayoutParams().width = dDimens[0];
        v.getLayoutParams().height = dDimens[1];
    }

    public int[] getDrawableDimensions(Context context, int id)
    {
        int[] dimensions = new int[2];
        if(context == null)
            return dimensions;
        Drawable d = getResources().getDrawable(id);
        if(d != null)
        {
            dimensions[0] = d.getIntrinsicWidth();
            dimensions[1] = d.getIntrinsicHeight();
        }
        return dimensions;
    }
}
