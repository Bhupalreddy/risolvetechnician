package self.sampleapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.sampleapp.adapters.CommunityUpdatesAdapter;
import self.sampleapp.adapters.ExclusiveOffersAdapter;
import self.sampleapp.model.Ad;
import self.sampleapp.model.CommunityUpdateDO;
import self.sampleapp.model.CommunityUpdates;
import self.sampleapp.model.ExclusiveOfferDO;
import self.sampleapp.model.ExclusiveOffers;
import self.sampleapp.model.Notice;
import self.sampleapp.web.WebAPI;

public class DashboardActivity extends AppCompatActivity {


    protected RecyclerView.LayoutManager lmExOffers, lmCommunityUpdates;
    protected ExclusiveOffersAdapter aExOffers;
    protected CommunityUpdatesAdapter aCommunityUpdates;
    protected ArrayList<ExclusiveOfferDO> doExOffers;
    protected ArrayList<CommunityUpdateDO> doCommunityUpdates;
    private CompositeDisposable disposables = new CompositeDisposable();

    @Inject
    SharedPreferences sPref;
    @Inject
    WebAPI mWebAPI;

    @BindView(R.id.rvExOffers) RecyclerView rvExOffers;
    @BindView(R.id.rvCommunityUpdates) RecyclerView rvCommunityUpdates;
    @BindView(R.id.tvNoExOf) TextView tvNoExOf;
    @BindView(R.id.tvNoCoUp) TextView tvNoCoUp;
    @BindView(R.id.tvSchedule) TextView tvSchedule;
    @BindView(R.id.tvComplaints) TextView tvComplaints;
    @BindView(R.id.tvReports) TextView tvReports;
    @BindView(R.id.tvHelp) TextView tvHelp;
    @BindView(R.id.tvProfile) TextView tvProfile;
    @BindView(R.id.tvTechnicianName) TextView tvTechnicianName;
    @BindView(R.id.ivBanner)
    ImageView ivBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
//        initDataSet();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        ((App) getApplication()).getNetComponent().inject(this);

        rvExOffers = (RecyclerView) findViewById(R.id.rvExOffers);
        lmExOffers = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvExOffers.setLayoutManager(lmExOffers);
        aExOffers = new ExclusiveOffersAdapter(null,DashboardActivity.this);
        rvExOffers.setAdapter(aExOffers);

        rvCommunityUpdates = (RecyclerView) findViewById(R.id.rvCommunityUpdates);
        lmCommunityUpdates = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCommunityUpdates.setLayoutManager(lmCommunityUpdates);
        aCommunityUpdates = new CommunityUpdatesAdapter(null, this, new CommunityUpdatesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Notice item) {
                Intent I = new Intent(DashboardActivity.this,CommunityDetailsActivity.class);
                I.putExtra("COMMUNITY",  item);
                startActivity(I);
            }
        });
        rvCommunityUpdates.setAdapter(aCommunityUpdates);
//        rvCommunityUpdates.setNestedScrollingEnabled(false);

        tvComplaints.setOnClickListener(v -> goToPage(0));

        tvSchedule.setOnClickListener(v -> goToPage(1));
        tvReports.setOnClickListener(v ->  goToPage(2));
        tvHelp.setOnClickListener(v -> goToPage(3));
        tvProfile.setOnClickListener(v->goToProfile());
        callWebForContent();
    }

    private void initDataSet(){
        doExOffers = new ArrayList<>();
        doExOffers.add(
                new ExclusiveOfferDO(R.drawable.e_1173_f_0,
                "Rent your Property",
                "Reach out to thousands of home seekers and get genuine leads.",
                "Starts from: 20k")
        );
        doExOffers.add(
                new ExclusiveOfferDO(R.drawable.e_1173_f_1,
                        "Aryamitra Bay Hills",
                        "Aryamitra is a real estate and urban infrastructure development company.",
                        "2,3 BHK Apartment")
        );
        doExOffers.add(
                new ExclusiveOfferDO(R.drawable.e_1173_f_2,
                        "Ongoing Projects",
                        "Reach out to thousands of home seekers and get genuine leads.",
                        "3BHK Starts from: 20k")
        );

        doCommunityUpdates = new ArrayList<>();
        doCommunityUpdates.add(new CommunityUpdateDO(R.drawable.e_1173_f_3,
                "Zumba Classes:",
                "Zumba sponsered by GHF & Mayors Council.",
                "When: Wednesday @ 6:30 PM to 7:30 PM",
                "Where: Commons Room"
        ));
        doCommunityUpdates.add(new CommunityUpdateDO(R.drawable.e_1173_f_4,
                "Yoga Classes:",
                "Yoga: Sponsored by GFH &amp; Mayors’ Council.",
                "When: Every Tuesday @ 6:30 PM to 7:30 PM",
                "Where: Recreation Room"
        ));
        doCommunityUpdates.add(new CommunityUpdateDO(R.drawable.e_1173_f_5,
                "Important Announcement:",
                "Tanglewood Pool Closed due to flooding.",
                "When: This weekend",
                "Where: Common Meeting Area"
        ));
    }

    private void callWebForContent(){

        String projectId = sPref.getString("bmaProjectId", "");

        Observable<ExclusiveOffers> callEOs = mWebAPI.getExclusiveOffers(projectId);
        Disposable callLogin = callEOs
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processEOs(x), x -> onError(x));
        disposables.add(callLogin);

        Observable<CommunityUpdates> call = mWebAPI.getCommunityUpdates(projectId, "10", "0");
        Disposable callCUs = call
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processCUs(x), x -> onError(x));
        disposables.add(callCUs);
    }

    private void processEOs(ExclusiveOffers exclusiveOffers){
        ArrayList<Ad> ads = exclusiveOffers.getAds();
        boolean isNonEmpty = ads != null && ads.size() > 0;
        rvCommunityUpdates.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
        tvNoCoUp.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
        aExOffers.refresh(ads);
    }

    private void processCUs(CommunityUpdates communityUpdates){
        ArrayList<Notice> notices = communityUpdates.getNotices();
        boolean isNonEmpty = notices != null && notices.size() > 0;
        rvExOffers.setVisibility(isNonEmpty ? View.VISIBLE: View.GONE);
        tvNoExOf.setVisibility(isNonEmpty ? View.GONE: View.VISIBLE);
        aCommunityUpdates.refresh(notices);
    }

    private void onError(Throwable throwable) {
        Snackbar.make(tvComplaints, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    private void goToPage(int page){
        Intent i = new Intent(DashboardActivity.this, ComplaintsActivity.class);
        i.putExtra("PAGE", page);
        startActivity(i);
    }

    private void goToProfile(){
        Intent i = new Intent(DashboardActivity.this, ProfileActivity.class);
        startActivity(i);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(disposables != null && disposables.size() > 0)
            disposables.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String imgUrl = sPref.getString("bmaProjectImage", null);
        String technicianName = sPref.getString("bmaUserName", null);
        Picasso.with(this)
                .load(imgUrl)
                .placeholder(R.drawable.banner)
                .into(ivBanner);
        tvTechnicianName.setText("Welcome "+technicianName);
    }
}
