package self.sampleapp.web;


import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import self.sampleapp.model.AcceptRejectResponseDO;
import self.sampleapp.model.CommunityUpdates;
import self.sampleapp.model.Complaints;
import self.sampleapp.model.EditProfileResponse;
import self.sampleapp.model.EstimatedImageUploadRequestDO;
import self.sampleapp.model.Estimations;
import self.sampleapp.model.ExclusiveOffers;
import self.sampleapp.model.LoginResponse;
import self.sampleapp.model.PostCloseComplaintResponse;
import self.sampleapp.model.PostEstimationRequestDO;
import self.sampleapp.model.PostEstimationResponseDO;
import self.sampleapp.model.Projects;
import self.sampleapp.model.Schedules;
import self.sampleapp.model.WorkOrders;

/**
 * Created by srikrishna on 10-07-2017.
 */

public interface  WebAPI {

    @FormUrlEncoded
    @POST("/risolve/webservices/login.php")
    Observable<LoginResponse> getLogin(@FieldMap Map<String, String> names);

    @POST("/risolve/webservices/projects.php")
    Observable<Projects> getProjects();

    @POST("/risolve/webservices/ads.php")
    Observable<ExclusiveOffers> getExclusiveOffers(@Query("project_id") String projectId);

    @POST("/risolve/webservices/notice-board.php")
    Observable<CommunityUpdates> getCommunityUpdates(
            @Query("project_id") String projectId,  // 1
            @Query("limit") String limit,           // 10
            @Query("offset") String offset);        // 0

    @POST("/risolve/webservices/post_complaint.php")
    Observable<Complaints> getComplaints(@Query("user_id") String userId);

    @GET("/risolve/webservices/assigned_complaint_list.php")
    Observable<Complaints> getTechnicianComplaintList(@Query("tech_id") String projectId);

    @GET("/risolve/webservices/ongoing_complaints.php")
    Observable<Complaints> getTechnicianOnGoingComplaintList(@Query("tech_id") String projectId);

    @FormUrlEncoded
    @POST("/risolve/webservices/accept_rejected.php")
    Observable<AcceptRejectResponseDO> acceptRejectComplaints(@FieldMap Map<String, String> names);

    @GET("/risolve/webservices/estimated_complaints.php")
    Observable<Estimations> getTechnicianEstimatedComplaintList(@Query("tech_id") String projectId);

    @GET("/risolve/webservices/workorder_complaints.php")
    Observable<WorkOrders> getTechnicianWorkOrderComplaintList(@Query("tech_id") String projectId);

    @GET("/risolve/webservices/ScheduleMaintenance.php")
    Observable<Schedules> getTechnicianScheduleMaintenanceList(@Query("limit") String limit, @Query("offset") String offset, @Query("project_id") String projectId);
    @FormUrlEncoded
    @POST("/risolve/webservices/forgotpassword.php")
    Observable<AcceptRejectResponseDO> forgotPassword(@FieldMap Map<String, String> names);
/*
{
  "tech_id": "5",
  "complaint_id": "6",
  "description_of_work": "description",
  "totalservicecast": "1000",
  "totalgoodscast": "1200",
  "total": "2200",
   "longitude": "13252",
   "latitude": "2345678",

  "estimation": [
    {
      "service": "vvvvvvvvvvvvvvvvvvvvv",
      "serviceAmount": "100",
      "qty": 10,
      "goods": "blub",
      "unitPrice": "10",
      "goodsAmount": "100"
    },
    {
      "service": "sssssssssssssssssss",
      "serviceAmount": "100",
      "qty": 10,
      "goods": "blub",
      "unitPrice": "10",
      "goodsAmount": "100"
    }
  ],
  "images": [
    {
      "url": "image name"
    },
    {"url": "image name"
    }
  ]
}
 */

   // @Multipart
   @Headers({
           "Content-Type: text/plain; charset=utf-8"
   })
    @POST("/risolve/webservices/post_estimation.php")
    Observable<PostEstimationResponseDO> giveEstimation(
            @Body PostEstimationRequestDO requestDO
            /*@Query("estimation[]") ArrayList<PostEstimation> estimations,
            @Part MultipartBody.Part[] estimation_image*/
            /*@Field("matirial_amount[]") ArrayList<Integer> matirial_amount*/);

    @Multipart
    @POST("/risolve/webservices/user-profile.php")
    Observable<EditProfileResponse> editProfile(@Part("bma_user_name") RequestBody userName,
                                                @Part("bma_user_mobile") RequestBody mobile,
                                                @Part("bma_user_id") RequestBody userId,
                                                @Part MultipartBody.Part image);

    /*
        "complaint_id=1
        technician_review=this problem is solved
        technician_uplode_images[]=images.jpg
        user_signature=siva.jpg
        complaint_status=7 [7 for closed]
        tech_signature=sivasignature.jpg"
         */
    @Multipart
    @POST("/risolve/webservices/close_complaint.php")
    Observable<PostCloseComplaintResponse> postCloseComplaint(
            @Part("complaint_id") RequestBody complaintId,
            @Part("technician_review") RequestBody technicianDesc,
            @Part("complaint_status") RequestBody complaintStatus,
            @Part MultipartBody.Part[] technician_uplode_images,
            @Part MultipartBody.Part user_signature,
            @Part MultipartBody.Part tech_signature
    );

    @Multipart
    @POST("/risolve/webservices/estimation_images.php")
    Observable<EstimatedImageUploadRequestDO> postEstimatedImagesUpload(
            @Part MultipartBody.Part[] complaint_image
    );

    }
