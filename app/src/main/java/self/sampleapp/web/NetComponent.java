package self.sampleapp.web;

import javax.inject.Singleton;

import dagger.Component;
import self.sampleapp.AuthenticateActivity;
import self.sampleapp.CommunityDetailsActivity;
import self.sampleapp.DashboardActivity;
import self.sampleapp.ExclusiveOfferDetailsActivity;
import self.sampleapp.ForgotPasswordActivity;
import self.sampleapp.ProfileActivity;
import self.sampleapp.fragments.ComplaintsFragment;
import self.sampleapp.fragments.CompliantsNewFragment;
import self.sampleapp.fragments.CompliantsOnGoingFragment;
import self.sampleapp.fragments.EstimatesFragment;
import self.sampleapp.fragments.EstimationAddImagesFragment;
import self.sampleapp.fragments.OrderEstimatesFragment;
import self.sampleapp.fragments.ScheduleMaintenanceFragment;
import self.sampleapp.fragments.ScheduledDetailsFragment;
import self.sampleapp.fragments.ScheduledMaintenanceDetailsFragment;
import self.sampleapp.fragments.ServiceReportsFragment;
import self.sampleapp.fragments.SubmitServiceReportOrCloseFragment;
import self.sampleapp.fragments.TechComplaintViewFragment;
import self.sampleapp.fragments.WorkOrdersFragment;

/**
 * Created by srikrishna on 11-07-2017.
 */

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(AuthenticateActivity activity);
    void inject(ForgotPasswordActivity activity);
    void inject(DashboardActivity activity);
    void inject(CommunityDetailsActivity activity);
    void inject(ExclusiveOfferDetailsActivity activity);
    void inject(ComplaintsFragment fragment);
    void inject(CompliantsNewFragment fragment);
    void inject(CompliantsOnGoingFragment fragment);
    void inject(EstimatesFragment fragment);
    void inject(WorkOrdersFragment fragment);
    void inject(ScheduleMaintenanceFragment fragment);
    void inject(ScheduledMaintenanceDetailsFragment fragment);
    void inject(ServiceReportsFragment fragment);
    void inject(TechComplaintViewFragment fragment);
    void inject(SubmitServiceReportOrCloseFragment fragment);
    void inject(OrderEstimatesFragment fragment);
    void inject(EstimationAddImagesFragment fragment);
    void inject(ScheduledDetailsFragment fragment);
    void inject(ProfileActivity activity);
}
