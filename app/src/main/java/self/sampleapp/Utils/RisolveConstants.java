package self.sampleapp.Utils;

/**
 * Created by PsSri on 7/15/2017.
 */

public class RisolveConstants {
    /*
    1 new complaint
    2 Assigned
    3 Estimated
    4 Ongoing
    5 Rejected
    6 Quarantine
    7 Closed
     */

    public static String COMPLAINT_STATUS_CODE_NEW ="1";
    public static String COMPLAINT_STATUS_CODE_ASSIGN ="2";
    public static String COMPLAINT_STATUS_CODE_ESTIMATED ="3";
    public static String COMPLAINT_STATUS_CODE_ONGOING ="4";
    public static String COMPLAINT_STATUS_CODE_REJECTED ="5";
    public static String COMPLAINT_STATUS_CODE_QUARANTINE ="6";
    public static String COMPLAINT_STATUS_CODE_CLOSED ="7";

}
