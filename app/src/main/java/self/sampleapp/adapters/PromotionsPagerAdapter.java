package self.sampleapp.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import self.sampleapp.R;


public class PromotionsPagerAdapter extends PagerAdapter {
	private Context context;
	private LayoutInflater inflater;
//	private int[] promtionImages = {R.drawable.one,R.drawable.two,R.drawable.three};

	public PromotionsPagerAdapter(Context context) {
		this.context = context;
	}

	@Override
	public int getCount()
	{
		return 3;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((RelativeLayout) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView ivPromotion;
		RelativeLayout itemView = null;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (position == 0){
			 itemView = (RelativeLayout) inflater.inflate(R.layout.intro_custom_layout1, container, false);
		}else if (position == 1){
			 itemView = (RelativeLayout) inflater.inflate(R.layout.intro_custom_layout2, container, false);
		}else if (position == 2){
			 itemView = (RelativeLayout) inflater.inflate(R.layout.intro_custom_layout3, container, false);
		}

//		ivPromotion = (ImageView) itemView.findViewById(R.id.ivPromotion);
//
//		Picasso.with(context).load(promtionImages[position]).into(ivPromotion);
		((ViewPager) container).addView(itemView);

		itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v)
			{

			}



		});



		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);
	}
}