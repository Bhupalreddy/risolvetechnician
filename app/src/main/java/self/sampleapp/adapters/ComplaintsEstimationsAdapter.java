package self.sampleapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import self.sampleapp.R;
import self.sampleapp.model.Estimation;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ComplaintsEstimationsAdapter extends RecyclerView.Adapter<ComplaintsEstimationsAdapter.ViewHolder> {
    private static final String TAG = "ExclusiveOffersAdapter";

    private ArrayList<Estimation> mDataSet;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvComplaintStatus, tvDate, tvTime, tvComplaintNumber, tvComplaintSubject, tvCompaintDepartment;
        public ImageView ivImage;

        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
            tvComplaintStatus = (TextView) v.findViewById(R.id.tvComplaintStatus);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvComplaintNumber = (TextView) v.findViewById(R.id.tvComplaintNumber);
            tvComplaintSubject = (TextView) v.findViewById(R.id.tvComplaintSubject);
            tvCompaintDepartment = (TextView) v.findViewById(R.id.tvCompaintDepartment);
        }
    }

    public ComplaintsEstimationsAdapter(ArrayList<Estimation> dataSet, Context context) {
        mDataSet = dataSet;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.complaint_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        Estimation eDO = mDataSet.get(position);
        if (eDO.getComplaintStatus().equals("2")){
            viewHolder.tvComplaintStatus.setText("ACCEPT / REJECT");
        }else if (eDO.getComplaintStatus().equals("3")){
            viewHolder.tvComplaintStatus.setText("VIEW / RE SUBMIT");
        }else if (eDO.getComplaintStatus().equals("4")){
            viewHolder.tvComplaintStatus.setText("ONGOING");
        }else if (eDO.getComplaintStatus().equals("7")){
            viewHolder.tvComplaintStatus.setText("CLOSED");
        }
        if (eDO.getBma_department_image() != null && !eDO.getBma_department_image().equalsIgnoreCase("")){
            String[] complaintImgs = eDO.getComplaintImages().split(",");
            Glide.with(context).load(eDO.getBma_department_image()).into(viewHolder.ivImage);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show pdf
                Intent browserEstimationPdfIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(eDO.getEstimation_pdf()));
                context.startActivity(browserEstimationPdfIntent);
            }
        });
        viewHolder.tvDate.setText(eDO.getComplaintCreatedDate());
        viewHolder.tvTime.setText(eDO.getComplaint_created_time());
        viewHolder.tvComplaintNumber.setText(eDO.getEstimation_complaint_id());
        viewHolder.tvComplaintSubject.setText(eDO.getComplaintDescription());
        viewHolder.tvCompaintDepartment.setText(eDO.getTechnicianDepartmentName());
//        Picasso.with(viewHolder.ivImage.getContext()).load(eDO.getImgRes()).into(viewHolder.ivImage);

    }

    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
