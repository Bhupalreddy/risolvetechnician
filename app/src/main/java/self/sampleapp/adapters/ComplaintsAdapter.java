package self.sampleapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import self.sampleapp.R;
import self.sampleapp.model.Complaint;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ComplaintsAdapter extends RecyclerView.Adapter<ComplaintsAdapter.ViewHolder> {
    private static final String TAG = "ExclusiveOffersAdapter";

    private ArrayList<Complaint> mDataSet;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvComplaintStatus, tvDate, tvTime, tvComplaintNumber, tvComplaintSubject, tvCompaintDepartment;
        public ImageView ivImage;
        public LinearLayout llContainer;
        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
            tvComplaintStatus = (TextView) v.findViewById(R.id.tvComplaintStatus);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvComplaintNumber = (TextView) v.findViewById(R.id.tvComplaintNumber);
            tvComplaintSubject = (TextView) v.findViewById(R.id.tvComplaintSubject);
            tvCompaintDepartment = (TextView) v.findViewById(R.id.tvCompaintDepartment);
            llContainer = (LinearLayout) v.findViewById(R.id.llContainer);
        }
    }

    public ComplaintsAdapter(ArrayList<Complaint> dataSet, Context context) {
        mDataSet = dataSet;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.complaint_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        Complaint eDO = mDataSet.get(position);
        viewHolder.tvComplaintStatus.setText(eDO.getComplaintStatus());
        viewHolder.tvDate.setText(eDO.getComplaintAvailabulityTime1());
        viewHolder.tvTime.setText(eDO.getComplaintAvailabulityTime1());
        viewHolder.tvComplaintNumber.setText(eDO.getComplaintId());
        viewHolder.tvComplaintSubject.setText(eDO.getComplaintSubject());
        viewHolder.tvCompaintDepartment.setText(eDO.getTechnicianDepartmentName());
//        Picasso.with(viewHolder.ivImage.getContext()).load(eDO.getImgRes()).into(viewHolder.ivImage);
        /*viewHolder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Complaint eDO = (Complaint) v.getTag();
                ScheduledMaintenanceDetailsFragment selectedFragment = ScheduledMaintenanceDetailsFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Complaint", eDO);
                selectedFragment.setArguments(bundle);
                FragmentTransaction transaction = ((ComplaintsActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content, selectedFragment).addToBackStack(null);
                transaction.commit();
            }
        });*/

    }

    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
