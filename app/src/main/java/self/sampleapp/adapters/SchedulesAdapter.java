package self.sampleapp.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import self.sampleapp.ComplaintsActivity;
import self.sampleapp.R;
import self.sampleapp.fragments.ScheduledDetailsFragment;
import self.sampleapp.model.Schedule;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class SchedulesAdapter extends RecyclerView.Adapter<SchedulesAdapter.ViewHolder> {
    private static final String TAG = "ExclusiveOffersAdapter";

    private ArrayList<Schedule> mDataSet;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvComplaintStatus, tvDate, tvTime, tvComplaintNumber, tvComplaintSubject, tvCompaintDepartment;
        public ImageView ivImage;
        public LinearLayout llContainer;

        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
            tvComplaintStatus = (TextView) v.findViewById(R.id.tvComplaintStatus);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvComplaintNumber = (TextView) v.findViewById(R.id.tvComplaintNumber);
            tvComplaintSubject = (TextView) v.findViewById(R.id.tvComplaintSubject);
            tvCompaintDepartment = (TextView) v.findViewById(R.id.tvCompaintDepartment);
            llContainer = (LinearLayout) v.findViewById(R.id.llContainer);
        }
    }

    public SchedulesAdapter(ArrayList<Schedule> dataSet, Context context) {
        mDataSet = dataSet;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.complaint_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        Schedule eDO = mDataSet.get(position);
        viewHolder.llContainer.setTag(eDO);
        /*if (eDO.getBma_schedule_status().equals("2")){
            viewHolder.tvComplaintStatus.setText("ASSIGNED");
        }else if (eDO.getBma_schedule_status().equals("3")){
            viewHolder.tvComplaintStatus.setText("ESTIMATED");
        }else if (eDO.getBma_schedule_status().equals("4")){
            viewHolder.tvComplaintStatus.setText("ONGOING");
        }else if (eDO.getBma_schedule_status().equals("7")){
            viewHolder.tvComplaintStatus.setText("CLOSED");
        }*/
//        viewHolder.tvComplaintStatus.setText(eDO.getBma_targeted_date());
        viewHolder.tvDate.setText(eDO.getBma_targeted_date());
        viewHolder.tvTime.setText(eDO.getBma_visited_days_months());
        viewHolder.tvComplaintNumber.setText(eDO.getBma_schedule_id());
        viewHolder.tvComplaintSubject.setText(eDO.getBma_schedule_name());
        viewHolder.tvCompaintDepartment.setText(eDO.getDepartment_name());
        viewHolder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Schedule eDO = (Schedule) v.getTag();
                ScheduledDetailsFragment selectedFragment = ScheduledDetailsFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Schedule", eDO);
                selectedFragment.setArguments(bundle);
                FragmentTransaction transaction = ((ComplaintsActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content, selectedFragment).addToBackStack(null);
                transaction.commit();
            }
        });
        /*if (eDO.getComplaintImages() != null && !eDO.getComplaintImages().equalsIgnoreCase("")){
            String[] complaintImgs = eDO.getComplaintImages().split(",");
            Picasso.with(viewHolder.ivImage.getContext()).load(complaintImgs[0]).into(viewHolder.ivImage);
        }*/
//        viewHolder.tvCompaintDepartment.setText(eDO.getTechnicianDepartmentName());
//        Picasso.with(viewHolder.ivImage.getContext()).load(eDO.getImgRes()).into(viewHolder.ivImage);

    }

    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
