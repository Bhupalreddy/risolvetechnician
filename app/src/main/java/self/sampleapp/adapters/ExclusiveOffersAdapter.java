package self.sampleapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import self.sampleapp.ExclusiveOfferDetailsActivity;
import self.sampleapp.R;
import self.sampleapp.model.Ad;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ExclusiveOffersAdapter extends RecyclerView.Adapter<ExclusiveOffersAdapter.ViewHolder> {
    private static final String TAG = "ExclusiveOffersAdapter";

    private ArrayList<Ad> mDataSet;
    private float dp;
    private Context mContext;
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvExOfferTitle, tvExOfferDesc, tvExOfferSub;
        public ImageView ivExOfferImage;

        public ViewHolder(View v) {
            super(v);
            ivExOfferImage = (ImageView) v.findViewById(R.id.ivExOfferImage);
            tvExOfferTitle = (TextView) v.findViewById(R.id.tvExOfferTitle);
            tvExOfferDesc = (TextView) v.findViewById(R.id.tvExOfferDesc);
            tvExOfferSub = (TextView) v.findViewById(R.id.tvExOfferSub);
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public ExclusiveOffersAdapter(ArrayList<Ad> dataSet,Context context) {
        mDataSet = dataSet;
        this.mContext = context;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1/* 1 pixel */, displaymetrics );
    }

    public void refresh(ArrayList<Ad> dataSet){
        mDataSet = dataSet;
        notifyDataSetChanged();
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.exclusive_offers_item, viewGroup, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        Ad eDO = mDataSet.get(position);
        viewHolder.tvExOfferTitle.setText(eDO.getBmaAdName());
        viewHolder.tvExOfferDesc.setText(eDO.getBmaAdDescription());
        viewHolder.tvExOfferSub.setText(eDO.getBmaProjectName());
        String imagePath = TextUtils.isEmpty(eDO.getBmaAdImage()) ? null : eDO.getBmaAdImage();
        Picasso.with(viewHolder.ivExOfferImage.getContext())
                .load(imagePath)
//                .resize((int)(110/dp), (int)(110/dp))
//                .centerCrop()
                .error(R.drawable.banner)
                .placeholder(R.drawable.empty_state)
                .into(viewHolder.ivExOfferImage);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent(mContext,ExclusiveOfferDetailsActivity.class);
                I.putExtra("EXCLUSIVE_OFFER",  eDO);
                mContext.startActivity(I);
            }
        });
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
