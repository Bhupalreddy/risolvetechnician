package self.sampleapp.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import self.sampleapp.R;

/**
 * Created by PsSri on 7/20/2017.
 */

public class ViewPagerAdapter extends PagerAdapter {
    Context context;
    String[] imageUrls;
    LayoutInflater inflater;

    public ViewPagerAdapter(Context context, String[] imageurls) {
        this.context = context;
        this.imageUrls = imageurls;
    }

    @Override
    public int getCount() {
        return imageUrls.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables
        ImageView imgFromUrlImageView;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container,
                false);

        // Locate the ImageView in viewpager_item.xml
        imgFromUrlImageView = (ImageView) itemView.findViewById(R.id.iv_from_url);
        // Capture position and set to the ImageView
        Picasso.with(context)
                .load(imageUrls[position])
                .placeholder(R.drawable.progress_animation)
                .into(imgFromUrlImageView);

        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((LinearLayout) object);

    }
}
