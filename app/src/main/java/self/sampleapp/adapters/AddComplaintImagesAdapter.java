package self.sampleapp.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import self.sampleapp.R;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class AddComplaintImagesAdapter extends RecyclerView.Adapter<AddComplaintImagesAdapter.ViewHolder> {
    private static final String TAG = "AddImagesAdapter";

    private ArrayList<Uri> mDataSet;
    private float density;
    private RecyclerView rv;
    private ImageView ivSelectImage;

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView ivCImage;
        public ImageButton btnClose;

        public ViewHolder(View v) {
            super(v);
            ivCImage = (ImageView) v.findViewById(R.id.ivCImage);
            btnClose = (ImageButton) v.findViewById(R.id.btnClose);
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public AddComplaintImagesAdapter(ArrayList<Uri> dataSet, RecyclerView rv, ImageView ivSelectImage) {
        mDataSet = dataSet;
        this.rv = rv;
        this.ivSelectImage = ivSelectImage;
        DisplayMetrics d = rv.getContext().getResources().getDisplayMetrics();
        density = d.density;
    }

    public void refresh(ArrayList<Uri> dataSet) {
        mDataSet = dataSet;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        mDataSet.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataSet.size());
        if (mDataSet.size() == 0) {
            rv.setVisibility(View.GONE);
            ivSelectImage.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.complaint_image_item, viewGroup, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        Uri uri = mDataSet.get(position);
        int size = (int) Math.abs(100 * density);
        Log.i("Size", " " + size);
        Log.d(TAG, "" + uri.getPath());
        Picasso.with(viewHolder.ivCImage.getContext())
                .load("file://"+uri.getPath())
                .resize(size, size)
                .centerCrop()
                .error(R.drawable.empty_state)
                .placeholder(R.drawable.empty_state)
                .into(viewHolder.ivCImage);
        viewHolder.btnClose.setOnClickListener((v) -> removeItem(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
