package self.sampleapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import self.sampleapp.ComplaintsActivity;
import self.sampleapp.R;
import self.sampleapp.fragments.TechComplaintViewFragment;
import self.sampleapp.model.Complaint;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class ComplaintsOnGoingAdapter extends RecyclerView.Adapter<ComplaintsOnGoingAdapter.ViewHolder> {
    private static final String TAG = "ExclusiveOffersAdapter";

    private ArrayList<Complaint> mDataSet;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvComplaintStatus, tvDate, tvTime, tvComplaintNumber, tvComplaintSubject, tvCompaintDepartment;
        public ImageView ivImage;
        private LinearLayout llContainer;
        public ViewHolder(View v) {
            super(v);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
            tvComplaintStatus = (TextView) v.findViewById(R.id.tvComplaintStatus);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvTime = (TextView) v.findViewById(R.id.tvTime);
            tvComplaintNumber = (TextView) v.findViewById(R.id.tvComplaintNumber);
            tvComplaintSubject = (TextView) v.findViewById(R.id.tvComplaintSubject);
            tvCompaintDepartment = (TextView) v.findViewById(R.id.tvCompaintDepartment);
            llContainer             = (LinearLayout) v.findViewById(R.id.llContainer);
        }
    }

    public ComplaintsOnGoingAdapter(ArrayList<Complaint> dataSet, Context context) {
        mDataSet = dataSet;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.complaint_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");
        /* Status codes
        2 Assigned
        3 Estimated
        4 Ongoing
        5 Rejected
        6 Quarantine
        7 Closed*/
        Complaint eDO = mDataSet.get(position);
        viewHolder.llContainer.setTag(eDO);
       /* if (eDO.getComplaintStatus().equals("2")){
            viewHolder.tvComplaintStatus.setText("ASSIGNED");
        }else if (eDO.getComplaintStatus().equals("3")){
            viewHolder.tvComplaintStatus.setText("ESTIMATED");
        }else if (eDO.getComplaintStatus().equals("4")){
            viewHolder.tvComplaintStatus.setText("ONGOING");
        }*/
        try{
            int status = Integer.parseInt(eDO.getComplaintStatus());
            viewHolder.tvComplaintStatus.setText(eDO.getBmaStatusName());
            viewHolder.tvComplaintStatus.setTextColor(Color.parseColor(statusColors[status - 1]));
        }
        catch (Exception e){}
        viewHolder.tvDate.setText(eDO.getComplaint_assigning_date());
        viewHolder.tvTime.setText(eDO.getComplaint_assigning_time());
        viewHolder.tvComplaintNumber.setText(eDO.getComplaintNumber());
        viewHolder.tvComplaintSubject.setText(eDO.getComplaintSubject());
        viewHolder.tvCompaintDepartment.setText(eDO.getTechnicianDepartmentName());
        if (eDO.getBma_department_image() != null && !eDO.getBma_department_image().equalsIgnoreCase("")){
            String[] complaintImgs = eDO.getComplaintImages().split(",");
            Glide.with(context).load(eDO.getBma_department_image()).into(viewHolder.ivImage);
        }
        viewHolder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Complaint eDO = (Complaint) v.getTag();
                TechComplaintViewFragment selectedFragment = TechComplaintViewFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putSerializable("Complaint", eDO);
                selectedFragment.setArguments(bundle);
                FragmentTransaction transaction = ((ComplaintsActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.content, selectedFragment).addToBackStack(null);
                transaction.commit();
            }
        });
//        Picasso.with(viewHolder.ivImage.getContext()).load(eDO.getImgRes()).into(viewHolder.ivImage);

    }
    private String[] statusColors = {
            "#2ecc71",
            "#2c9bd9",
            "#d35400",
            "#8e44ad",
            "#95a5a6",
            "#95a5a6",
            "#95a5a6"
    };
    @Override
    public int getItemCount() {
        if (mDataSet != null && mDataSet.size() > 0)
            return mDataSet.size();
        else return 0;
    }
}
