package self.sampleapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import self.sampleapp.Utils.BottomNavigationViewHelper;
import self.sampleapp.fragments.ComplaintsFragment;
import self.sampleapp.fragments.HelpFragment;
import self.sampleapp.fragments.ReportsFragment;
import self.sampleapp.fragments.ScheduleMaintenanceFragment;


public class ComplaintsActivity extends AppCompatActivity {

    private TextView tvEdit;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_schedule:
                    selectedFragment = ScheduleMaintenanceFragment.newInstance();
//                    getSupportActionBar().setTitle("Schedule Maintenance");
                    tvEdit.setText("Schedule Maintenance");
                    break;
                case R.id.navigation_complaints:
                    selectedFragment = ComplaintsFragment
                            .newInstance();
                    tvEdit.setText("Complaints");
//                    getSupportActionBar().setTitle("Complaints");
                    break;
                case R.id.navigation_reports:
                    selectedFragment = ReportsFragment.newInstance();
//                    getSupportActionBar().setTitle("Reports");
                    tvEdit.setText("Reports");
                    break;
                case R.id.navigation_help:
                    selectedFragment = HelpFragment.newInstance();
//                    getSupportActionBar().setTitle("Help");
                    tvEdit.setText("Help");
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, selectedFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);

        // http://www.truiton.com/2017/01/android-bottom-navigation-bar-example/
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvEdit = (TextView) toolbar.findViewById(R.id.tvEdit);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        int pos = getIntent().getIntExtra("PAGE", 0);

        //Manually displaying the first fragment - one time only
        switch (pos) {
            case 0:
                navigation.setSelectedItemId(R.id.navigation_complaints);
                break;
            case 1:
                navigation.setSelectedItemId(R.id.navigation_schedule);
                break;
            case 2:
                navigation.setSelectedItemId(R.id.navigation_reports);
                break;
            case 3:
                navigation.setSelectedItemId(R.id.navigation_help);
                break;
        }
    }

}
