package self.sampleapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.sampleapp.Utils.Validators;
import self.sampleapp.model.AcceptRejectResponseDO;
import self.sampleapp.model.LoginResponse;
import self.sampleapp.web.WebAPI;


public class ForgotPasswordActivity extends AppCompatActivity {

    @Inject
    WebAPI mWebAPI;
    @Inject SharedPreferences sPref;
    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.tvForgotPWD)
    TextView tvForgotPWD;

    private CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pwd);
        ButterKnife.bind(this);
        ((App) getApplication()).getNetComponent().inject(this);

        tvForgotPWD.setOnClickListener(v -> callForgotPWDService());
    }


    private void callForgotPWDService() {
        if (validateLogin()) {
            Map<String, String> names = generateMap();
            Observable<AcceptRejectResponseDO> call = mWebAPI.forgotPassword(names);

            Disposable callLogin = call
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(loginResponse -> onSuccess(loginResponse),
                            throwable -> onError(throwable));
            disposables.add(callLogin);
        }
    }

    private Map<String, String> generateMap() {
        String email = etEmail.getText().toString();
        Map<String, String> names = new HashMap<>();
        names.put("email", email);
        return names;
    }

    private boolean validateLogin() {
        String email = etEmail.getText().toString();
        boolean isEmailEmpty = TextUtils.isEmpty(email);
        if (isEmailEmpty) {
            Snackbar.make(etEmail, "Email should not be empty.", Snackbar.LENGTH_SHORT).show();
        } else if (!Validators.validate(email)) {
            Snackbar.make(etEmail, "Enter a proper Email Address.", Snackbar.LENGTH_SHORT).show();
        } else {
            return true;
        }
        return false;
    }

    private void onSuccess(AcceptRejectResponseDO loginResponse) {

        // DO Something with Login Response
            finish();

    }

    private void onError(Throwable throwable) {
        Snackbar.make(etEmail, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposables != null && disposables.size() > 0)
            disposables.clear();
    }
}
