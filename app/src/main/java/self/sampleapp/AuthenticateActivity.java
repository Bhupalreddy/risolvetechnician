package self.sampleapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import self.sampleapp.Utils.Validators;
import self.sampleapp.model.LoginResponse;
import self.sampleapp.model.Project;
import self.sampleapp.model.Projects;
import self.sampleapp.web.WebAPI;


public class AuthenticateActivity extends AppCompatActivity {

    @Inject
    WebAPI mWebAPI;
    @Inject SharedPreferences sPref;
    @BindView(R.id.btnSignIn) Button btnSignIn;
    @BindView(R.id.btnForgotPWD) Button btnForgotPWD;
    @BindView(R.id.etLogin) EditText etLogin;
    @BindView(R.id.etPassword) EditText etPassword;
    @BindView(R.id.spinner) Spinner spinner;

    private CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authnicate);
        ButterKnife.bind(this);
        ((App) getApplication()).getNetComponent().inject(this);

        btnSignIn.setOnClickListener(v -> callLoginService());
        btnForgotPWD.setOnClickListener(v -> navigateToForgotPWD());

        Observable<Projects> call = mWebAPI.getProjects();
        Disposable callProjects = call
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(projects -> processProjects(projects),
                        throwable -> onError(throwable));
        disposables.add(callProjects);
    }

    private void processProjects(Projects projects){
        ArrayList<String> projectNames = new ArrayList<>();
        for (Project project: projects.getProjects()) {
            projectNames.add(project.getBmaProjectName());
        }
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.area_spinner_tv, projectNames);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    private void navigateToForgotPWD() {
        Intent in = new Intent(AuthenticateActivity.this, ForgotPasswordActivity.class);
        startActivity(in);
    }

    private void callLoginService() {
        if (validateLogin()) {
            Map<String, String> names = generateMap();
            Observable<LoginResponse> call = mWebAPI.getLogin(names);

            Disposable callLogin = call
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(loginResponse -> onSuccess(loginResponse),
                            throwable -> onError(throwable));
            disposables.add(callLogin);
        }
    }

    private Map<String, String> generateMap() {
        String email = etLogin.getText().toString();
        String password = etPassword.getText().toString();
        Map<String, String> names = new HashMap<>();
        names.put("username", email);
        names.put("password", password);
        return names;
    }

    private boolean validateLogin() {
        String email = etLogin.getText().toString();
        String password = etPassword.getText().toString();
        boolean isEmailEmpty = TextUtils.isEmpty(email);
        boolean isPasswordEmpty = TextUtils.isEmpty(password);
        if (isEmailEmpty) {
            Snackbar.make(etLogin, "Email should not be empty.", Snackbar.LENGTH_SHORT).show();
        } else if (!Validators.validate(email)) {
            Snackbar.make(etLogin, "Enter a proper Email Address.", Snackbar.LENGTH_SHORT).show();
        } else if (isPasswordEmpty) {
            Snackbar.make(etLogin, "Password should not be empty.", Snackbar.LENGTH_SHORT).show();
        } else if (password.length() < 4) {
            Snackbar.make(etLogin, "Password should be atleast 4 charecters", Snackbar.LENGTH_SHORT).show();
        } else {
            return true;
        }
        return false;
    }

    private void onSuccess(LoginResponse loginResponse) {

        // DO Something with Login Response
        SharedPreferences.Editor e = sPref.edit();
        e.putString("bmaUserId", loginResponse.getOutput().getBmaUserId());
        e.putString("bmaUserName", loginResponse.getOutput().getBmaUserName());
        e.putString("bmaUserEmail", loginResponse.getOutput().getBmaUserEmail());
        e.putString("bmaUserMobile", loginResponse.getOutput().getBmaUserMobile());
        e.putString("bmaProjectAddress", loginResponse.getOutput().getBmaProjectAddress());
        e.putString("bmaProjectImage", loginResponse.getOutput().getBmaProjectImage());
        e.putString("bmaProjectId", loginResponse.getOutput().getBmaProjectId());
        e.putString("bmaUserType", loginResponse.getOutput().getBmaUserTypeId());

        e.commit();

        Log.i("Login Response", loginResponse.toString());
        if(loginResponse.getOutput().getBmaUserTypeId().equalsIgnoreCase("4")){
            startActivity(new Intent(AuthenticateActivity.this, DashboardActivity.class));
            finish();
        }else if(loginResponse.getOutput().getBmaUserTypeId().equalsIgnoreCase("5")){
            startActivity(new Intent(AuthenticateActivity.this, DashboardActivity.class));
            finish();
        }

    }

    private void onError(Throwable throwable) {
        Snackbar.make(etLogin, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposables != null && disposables.size() > 0)
            disposables.clear();
    }
}
