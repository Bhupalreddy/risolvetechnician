package self.sampleapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import self.sampleapp.model.Ad;
import self.sampleapp.web.WebAPI;

public class ExclusiveOfferDetailsActivity extends AppCompatActivity {

    @Inject
    SharedPreferences sPref;
    @Inject
    WebAPI mWebAPI;

    @BindView(R.id.ivCoummityBanner)
    ImageView ivCommunityBanner;
    @BindView(R.id.tvCoummityTitle)
    TextView communityTitleTextView;
    @BindView(R.id.tvCoummityDescription)
    TextView communityDesTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.community_updates_details_fragment);
        ButterKnife.bind(this);
        ((App) getApplication()).getNetComponent().inject(this);
        Intent i = getIntent();
        Ad excusiveOffer = (Ad) i.getSerializableExtra("EXCLUSIVE_OFFER");
        getSupportActionBar().setTitle(excusiveOffer.getBmaAdName());
        communityTitleTextView.setText(excusiveOffer.getBmaAdName());
        communityDesTextView.setText(excusiveOffer.getBmaAdDescription());
        Picasso.with(this)
                .load(excusiveOffer.getBmaAdImage())
                .placeholder(R.drawable.banner)
                .into(ivCommunityBanner);
    }
}
