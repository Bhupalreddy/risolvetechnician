package self.sampleapp.model;

import java.io.Serializable;

/**
 * Created by srikrishna on 04-07-2017.
 */

public class CommunityUpdateDO implements Serializable {

    public CommunityUpdateDO(int imgRes, String title, String description, String when, String where){
        this.imgRes = imgRes;
        this.title = title;
        this.description = description;
        this.when = when;
        this.where = where;
    }

    private int imgRes;
    private String title;
    private String description;
    private String when;
    private String where;

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }
}
