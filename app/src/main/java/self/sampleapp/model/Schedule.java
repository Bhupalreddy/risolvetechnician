
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Schedule implements Serializable{

    @SerializedName("bma_schedule_id")
    @Expose
    private String bma_schedule_id;
    @SerializedName("bma_schedule_name")
    @Expose
    private String bma_schedule_name;
    @SerializedName("bma_contractor_id")
    @Expose
    private String bma_contractor_id;
    @SerializedName("bma_schedule_project_id")
    @Expose
    private String bma_schedule_project_id;
    @SerializedName("bma_visit_frequency")
    @Expose
    private String bma_visit_frequency;
    @SerializedName("bma_visited_days_months")
    @Expose
    private String bma_visited_days_months;
    @SerializedName("bma_targeted_date")
    @Expose
    private String bma_targeted_date;
    @SerializedName("bma_inchrge_name")
    @Expose
    private String bma_inchrge_name;
    @SerializedName("bma_incharge_phone")
    @Expose
    private String bma_incharge_phone;
    @SerializedName("bma_incharge_email_id")
    @Expose
    private String bma_incharge_email_id;
    @SerializedName("bma_schedule_status")
    @Expose
    private String bma_schedule_status;
    @SerializedName("bma_project_name")
    @Expose
    private String bma_project_name;
    @SerializedName("contractor_company_name")
    @Expose
    private String contractor_company_name;
    @SerializedName("contractor_company_address")
    @Expose
    private String contractor_company_address;
    @SerializedName("contractor_company_phone")
    @Expose
    private String contractor_company_phone;
    @SerializedName("contractor_company_mail")
    @Expose
    private String contractor_company_mail;
    @SerializedName("contractor_company_logo")
    @Expose
    private String contractor_company_logo;
    @SerializedName("contractor_main_person_name")
    @Expose
    private String contractor_main_person_name;
    @SerializedName("contractor_main_person_email")
    @Expose
    private String contractor_main_person_email;
    @SerializedName("contractor_main_person_phone")
    @Expose
    private String contractor_main_person_phone;
    @SerializedName("department_name")
    @Expose
    private String department_name;

    public String getBma_schedule_id() {
        return bma_schedule_id;
    }

    public void setBma_schedule_id(String bma_schedule_id) {
        this.bma_schedule_id = bma_schedule_id;
    }

    public String getBma_schedule_name() {
        return bma_schedule_name;
    }

    public void setBma_schedule_name(String bma_schedule_name) {
        this.bma_schedule_name = bma_schedule_name;
    }

    public String getBma_contractor_id() {
        return bma_contractor_id;
    }

    public void setBma_contractor_id(String bma_contractor_id) {
        this.bma_contractor_id = bma_contractor_id;
    }

    public String getBma_schedule_project_id() {
        return bma_schedule_project_id;
    }

    public void setBma_schedule_project_id(String bma_schedule_project_id) {
        this.bma_schedule_project_id = bma_schedule_project_id;
    }

    public String getBma_visit_frequency() {
        return bma_visit_frequency;
    }

    public void setBma_visit_frequency(String bma_visit_frequency) {
        this.bma_visit_frequency = bma_visit_frequency;
    }

    public String getBma_visited_days_months() {
        return bma_visited_days_months;
    }

    public void setBma_visited_days_months(String bma_visited_days_months) {
        this.bma_visited_days_months = bma_visited_days_months;
    }

    public String getBma_targeted_date() {
        return bma_targeted_date;
    }

    public void setBma_targeted_date(String bma_targeted_date) {
        this.bma_targeted_date = bma_targeted_date;
    }

    public String getBma_inchrge_name() {
        return bma_inchrge_name;
    }

    public void setBma_inchrge_name(String bma_inchrge_name) {
        this.bma_inchrge_name = bma_inchrge_name;
    }

    public String getBma_incharge_phone() {
        return bma_incharge_phone;
    }

    public void setBma_incharge_phone(String bma_incharge_phone) {
        this.bma_incharge_phone = bma_incharge_phone;
    }

    public String getBma_incharge_email_id() {
        return bma_incharge_email_id;
    }

    public void setBma_incharge_email_id(String bma_incharge_email_id) {
        this.bma_incharge_email_id = bma_incharge_email_id;
    }

    public String getBma_schedule_status() {
        return bma_schedule_status;
    }

    public void setBma_schedule_status(String bma_schedule_status) {
        this.bma_schedule_status = bma_schedule_status;
    }

    public String getBma_project_name() {
        return bma_project_name;
    }

    public void setBma_project_name(String bma_project_name) {
        this.bma_project_name = bma_project_name;
    }

    public String getContractor_company_name() {
        return contractor_company_name;
    }

    public void setContractor_company_name(String contractor_company_name) {
        this.contractor_company_name = contractor_company_name;
    }

    public String getContractor_company_address() {
        return contractor_company_address;
    }

    public void setContractor_company_address(String contractor_company_address) {
        this.contractor_company_address = contractor_company_address;
    }

    public String getContractor_company_phone() {
        return contractor_company_phone;
    }

    public void setContractor_company_phone(String contractor_company_phone) {
        this.contractor_company_phone = contractor_company_phone;
    }

    public String getContractor_company_mail() {
        return contractor_company_mail;
    }

    public void setContractor_company_mail(String contractor_company_mail) {
        this.contractor_company_mail = contractor_company_mail;
    }

    public String getContractor_company_logo() {
        return contractor_company_logo;
    }

    public void setContractor_company_logo(String contractor_company_logo) {
        this.contractor_company_logo = contractor_company_logo;
    }

    public String getContractor_main_person_name() {
        return contractor_main_person_name;
    }

    public void setContractor_main_person_name(String contractor_main_person_name) {
        this.contractor_main_person_name = contractor_main_person_name;
    }

    public String getContractor_main_person_email() {
        return contractor_main_person_email;
    }

    public void setContractor_main_person_email(String contractor_main_person_email) {
        this.contractor_main_person_email = contractor_main_person_email;
    }

    public String getContractor_main_person_phone() {
        return contractor_main_person_phone;
    }

    public void setContractor_main_person_phone(String contractor_main_person_phone) {
        this.contractor_main_person_phone = contractor_main_person_phone;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }
}
