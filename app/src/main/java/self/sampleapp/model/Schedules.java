
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Schedules {

    @SerializedName("schedule")
    @Expose
    private ArrayList<Schedule> schedule = null;
    @SerializedName("msg_code")
    @Expose
    private Integer msgCode;
    @SerializedName("msg")
    @Expose
    private String msg;

    public ArrayList<Schedule> getComplaints() {
        return schedule;
    }

    public void setComplaints(ArrayList<Schedule> complaints) {
        this.schedule = schedule;
    }

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
