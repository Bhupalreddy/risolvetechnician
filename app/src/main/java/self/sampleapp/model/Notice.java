
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Notice implements Serializable{

    @SerializedName("bma_notice_id")
    @Expose
    private String bmaNoticeId;
    @SerializedName("bma_notice_name")
    @Expose
    private String bmaNoticeName;
    @SerializedName("bma_notice_description")
    @Expose
    private String bmaNoticeDescription;
    @SerializedName("bma_notice_num_days")
    @Expose
    private String bmaNoticeNumDays;
    @SerializedName("bma_notice_image")
    @Expose
    private String bmaNoticeImage;
    @SerializedName("bma_notice_created_date")
    @Expose
    private String bmaNoticeCreatedDate;
    @SerializedName("bma_notice_updated_date")
    @Expose
    private String bmaNoticeUpdatedDate;
    @SerializedName("bma_notice_status")
    @Expose
    private String bmaNoticeStatus;
    @SerializedName("bma_notice_project_id")
    @Expose
    private String bmaNoticeProjectId;
    @SerializedName("bma_project_name")
    @Expose
    private String bmaProjectName;
    @SerializedName("bma_project_address")
    @Expose
    private String bmaProjectAddress;
    @SerializedName("bma_project_longitude")
    @Expose
    private String bmaProjectLongitude;
    @SerializedName("bma_project_latitude")
    @Expose
    private String bmaProjectLatitude;
    @SerializedName("bma_project_created_date")
    @Expose
    private String bmaProjectCreatedDate;
    @SerializedName("bma_project_updated_date")
    @Expose
    private String bmaProjectUpdatedDate;
    @SerializedName("bma_project_status")
    @Expose
    private String bmaProjectStatus;

    public String getBmaNoticeId() {
        return bmaNoticeId;
    }

    public void setBmaNoticeId(String bmaNoticeId) {
        this.bmaNoticeId = bmaNoticeId;
    }

    public String getBmaNoticeName() {
        return bmaNoticeName;
    }

    public void setBmaNoticeName(String bmaNoticeName) {
        this.bmaNoticeName = bmaNoticeName;
    }

    public String getBmaNoticeDescription() {
        return bmaNoticeDescription;
    }

    public void setBmaNoticeDescription(String bmaNoticeDescription) {
        this.bmaNoticeDescription = bmaNoticeDescription;
    }

    public String getBmaNoticeNumDays() {
        return bmaNoticeNumDays;
    }

    public void setBmaNoticeNumDays(String bmaNoticeNumDays) {
        this.bmaNoticeNumDays = bmaNoticeNumDays;
    }

    public String getBmaNoticeImage() {
        return bmaNoticeImage;
    }

    public void setBmaNoticeImage(String bmaNoticeImage) {
        this.bmaNoticeImage = bmaNoticeImage;
    }

    public String getBmaNoticeCreatedDate() {
        return bmaNoticeCreatedDate;
    }

    public void setBmaNoticeCreatedDate(String bmaNoticeCreatedDate) {
        this.bmaNoticeCreatedDate = bmaNoticeCreatedDate;
    }

    public String getBmaNoticeUpdatedDate() {
        return bmaNoticeUpdatedDate;
    }

    public void setBmaNoticeUpdatedDate(String bmaNoticeUpdatedDate) {
        this.bmaNoticeUpdatedDate = bmaNoticeUpdatedDate;
    }

    public String getBmaNoticeStatus() {
        return bmaNoticeStatus;
    }

    public void setBmaNoticeStatus(String bmaNoticeStatus) {
        this.bmaNoticeStatus = bmaNoticeStatus;
    }

    public String getBmaNoticeProjectId() {
        return bmaNoticeProjectId;
    }

    public void setBmaNoticeProjectId(String bmaNoticeProjectId) {
        this.bmaNoticeProjectId = bmaNoticeProjectId;
    }

    public String getBmaProjectName() {
        return bmaProjectName;
    }

    public void setBmaProjectName(String bmaProjectName) {
        this.bmaProjectName = bmaProjectName;
    }

    public String getBmaProjectAddress() {
        return bmaProjectAddress;
    }

    public void setBmaProjectAddress(String bmaProjectAddress) {
        this.bmaProjectAddress = bmaProjectAddress;
    }

    public String getBmaProjectLongitude() {
        return bmaProjectLongitude;
    }

    public void setBmaProjectLongitude(String bmaProjectLongitude) {
        this.bmaProjectLongitude = bmaProjectLongitude;
    }

    public String getBmaProjectLatitude() {
        return bmaProjectLatitude;
    }

    public void setBmaProjectLatitude(String bmaProjectLatitude) {
        this.bmaProjectLatitude = bmaProjectLatitude;
    }

    public String getBmaProjectCreatedDate() {
        return bmaProjectCreatedDate;
    }

    public void setBmaProjectCreatedDate(String bmaProjectCreatedDate) {
        this.bmaProjectCreatedDate = bmaProjectCreatedDate;
    }

    public String getBmaProjectUpdatedDate() {
        return bmaProjectUpdatedDate;
    }

    public void setBmaProjectUpdatedDate(String bmaProjectUpdatedDate) {
        this.bmaProjectUpdatedDate = bmaProjectUpdatedDate;
    }

    public String getBmaProjectStatus() {
        return bmaProjectStatus;
    }

    public void setBmaProjectStatus(String bmaProjectStatus) {
        this.bmaProjectStatus = bmaProjectStatus;
    }

}
