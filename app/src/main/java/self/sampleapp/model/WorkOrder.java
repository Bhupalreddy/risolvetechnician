
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WorkOrder implements Serializable{

    @SerializedName("workorder_id")
    @Expose
    private String workorder_id;
    @SerializedName("workorder_complaint_id")
    @Expose
    private String workorder_complaint_id;
    @SerializedName("workorder_estimation_id")
    @Expose
    private String workorder_estimation_id;
    @SerializedName("workorder_user_acceptence")
    @Expose
    private String workorder_user_acceptence;
    @SerializedName("workorder_created_date")
    @Expose
    private String workorder_created_date;
    @SerializedName("workorder_updated_date")
    @Expose
    private String workorder_updated_date;
    @SerializedName("complaint_id")
    @Expose
    private String complaintId;
    @SerializedName("complaint_number")
    @Expose
    private String complaintNumber;
    @SerializedName("complaint_user_id")
    @Expose
    private String complaintUserId;
    @SerializedName("complaint_subject")
    @Expose
    private String complaintSubject;
    @SerializedName("complaint_description")
    @Expose
    private String complaintDescription;
    @SerializedName("complaint_department_id")
    @Expose
    private String complaintDepartmentId;
    @SerializedName("complaint_images")
    @Expose
    private String complaintImages;
    @SerializedName("complaint_videos")
    @Expose
    private String complaintVideos;
    @SerializedName("complaint_availabulity_time1")
    @Expose
    private String complaintAvailabulityTime1;
    @SerializedName("complaint_availabulity_time2")
    @Expose
    private String complaintAvailabulityTime2;
    @SerializedName("complaint_acceptence_id")
    @Expose
    private Object complaintAcceptenceId;
    @SerializedName("complaint_user_feedback")
    @Expose
    private Object complaintUserFeedback;
    @SerializedName("complaint_technician_review")
    @Expose
    private Object complaintTechnicianReview;
    @SerializedName("complaint_technician_uplode_images")
    @Expose
    private Object complaintTechnicianUplodeImages;
    @SerializedName("complaint_user_signature")
    @Expose
    private Object complaintUserSignature;
    @SerializedName("complaint_estimated_id")
    @Expose
    private Object complaintEstimatedId;
    @SerializedName("complaint_assigned_id")
    @Expose
    private Object complaintAssignedId;
    @SerializedName("complaint_created_date")
    @Expose
    private String complaintCreatedDate;
    @SerializedName("complaint_updated_date")
    @Expose
    private String complaintUpdatedDate;
    @SerializedName("complaint_status")
    @Expose
    private String complaintStatus;
    @SerializedName("TechnicianName")
    @Expose
    private String TechnicianName;
    @SerializedName("TechnicianMobile")
    @Expose
    private String TechnicianMobile;
    @SerializedName("TechnicianImage")
    @Expose
    private String TechnicianImage;
    @SerializedName("TechnicianDepartmentName")
    @Expose
    private String TechnicianDepartmentName;
    @SerializedName("UserProject")
    @Expose
    private String UserProject;
    @SerializedName("UserBuilding")
    @Expose
    private String UserBuilding;
    @SerializedName("UserFlat")
    @Expose
    private String UserFlat;
    @SerializedName("UserName")
    @Expose
    private String UserName;
    @SerializedName("UserMobile")
    @Expose
    private String UserMobile;
    @SerializedName("workorder_created_time")
    @Expose
    private String workorder_created_time;

    public String getWorkorder_id() {
        return workorder_id;
    }

    public void setWorkorder_id(String workorder_id) {
        this.workorder_id = workorder_id;
    }

    public String getWorkorder_complaint_id() {
        return workorder_complaint_id;
    }

    public void setWorkorder_complaint_id(String workorder_complaint_id) {
        this.workorder_complaint_id = workorder_complaint_id;
    }

    public String getWorkorder_estimation_id() {
        return workorder_estimation_id;
    }

    public void setWorkorder_estimation_id(String workorder_estimation_id) {
        this.workorder_estimation_id = workorder_estimation_id;
    }

    public String getWorkorder_user_acceptence() {
        return workorder_user_acceptence;
    }

    public void setWorkorder_user_acceptence(String workorder_user_acceptence) {
        this.workorder_user_acceptence = workorder_user_acceptence;
    }

    public String getWorkorder_created_date() {
        return workorder_created_date;
    }

    public void setWorkorder_created_date(String workorder_created_date) {
        this.workorder_created_date = workorder_created_date;
    }

    public String getWorkorder_updated_date() {
        return workorder_updated_date;
    }

    public void setWorkorder_updated_date(String workorder_updated_date) {
        this.workorder_updated_date = workorder_updated_date;
    }

    public String getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(String complaintId) {
        this.complaintId = complaintId;
    }

    public String getComplaintNumber() {
        return complaintNumber;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public String getComplaintUserId() {
        return complaintUserId;
    }

    public void setComplaintUserId(String complaintUserId) {
        this.complaintUserId = complaintUserId;
    }

    public String getComplaintSubject() {
        return complaintSubject;
    }

    public void setComplaintSubject(String complaintSubject) {
        this.complaintSubject = complaintSubject;
    }

    public String getComplaintDescription() {
        return complaintDescription;
    }

    public void setComplaintDescription(String complaintDescription) {
        this.complaintDescription = complaintDescription;
    }

    public String getComplaintDepartmentId() {
        return complaintDepartmentId;
    }

    public void setComplaintDepartmentId(String complaintDepartmentId) {
        this.complaintDepartmentId = complaintDepartmentId;
    }

    public String getComplaintImages() {
        return complaintImages;
    }

    public void setComplaintImages(String complaintImages) {
        this.complaintImages = complaintImages;
    }

    public String getComplaintVideos() {
        return complaintVideos;
    }

    public void setComplaintVideos(String complaintVideos) {
        this.complaintVideos = complaintVideos;
    }

    public String getComplaintAvailabulityTime1() {
        return complaintAvailabulityTime1;
    }

    public void setComplaintAvailabulityTime1(String complaintAvailabulityTime1) {
        this.complaintAvailabulityTime1 = complaintAvailabulityTime1;
    }

    public String getComplaintAvailabulityTime2() {
        return complaintAvailabulityTime2;
    }

    public void setComplaintAvailabulityTime2(String complaintAvailabulityTime2) {
        this.complaintAvailabulityTime2 = complaintAvailabulityTime2;
    }

    public Object getComplaintAcceptenceId() {
        return complaintAcceptenceId;
    }

    public void setComplaintAcceptenceId(Object complaintAcceptenceId) {
        this.complaintAcceptenceId = complaintAcceptenceId;
    }

    public Object getComplaintUserFeedback() {
        return complaintUserFeedback;
    }

    public void setComplaintUserFeedback(Object complaintUserFeedback) {
        this.complaintUserFeedback = complaintUserFeedback;
    }

    public Object getComplaintTechnicianReview() {
        return complaintTechnicianReview;
    }

    public void setComplaintTechnicianReview(Object complaintTechnicianReview) {
        this.complaintTechnicianReview = complaintTechnicianReview;
    }

    public Object getComplaintTechnicianUplodeImages() {
        return complaintTechnicianUplodeImages;
    }

    public void setComplaintTechnicianUplodeImages(Object complaintTechnicianUplodeImages) {
        this.complaintTechnicianUplodeImages = complaintTechnicianUplodeImages;
    }

    public Object getComplaintUserSignature() {
        return complaintUserSignature;
    }

    public void setComplaintUserSignature(Object complaintUserSignature) {
        this.complaintUserSignature = complaintUserSignature;
    }

    public Object getComplaintEstimatedId() {
        return complaintEstimatedId;
    }

    public void setComplaintEstimatedId(Object complaintEstimatedId) {
        this.complaintEstimatedId = complaintEstimatedId;
    }

    public Object getComplaintAssignedId() {
        return complaintAssignedId;
    }

    public void setComplaintAssignedId(Object complaintAssignedId) {
        this.complaintAssignedId = complaintAssignedId;
    }

    public String getComplaintCreatedDate() {
        return complaintCreatedDate;
    }

    public void setComplaintCreatedDate(String complaintCreatedDate) {
        this.complaintCreatedDate = complaintCreatedDate;
    }

    public String getComplaintUpdatedDate() {
        return complaintUpdatedDate;
    }

    public void setComplaintUpdatedDate(String complaintUpdatedDate) {
        this.complaintUpdatedDate = complaintUpdatedDate;
    }

    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public String getTechnicianName() {
        return TechnicianName;
    }

    public void setTechnicianName(String technicianName) {
        TechnicianName = technicianName;
    }

    public String getTechnicianMobile() {
        return TechnicianMobile;
    }

    public void setTechnicianMobile(String technicianMobile) {
        TechnicianMobile = technicianMobile;
    }

    public String getTechnicianImage() {
        return TechnicianImage;
    }

    public void setTechnicianImage(String technicianImage) {
        TechnicianImage = technicianImage;
    }

    public String getTechnicianDepartmentName() {
        return TechnicianDepartmentName;
    }

    public void setTechnicianDepartmentName(String technicianDepartmentName) {
        TechnicianDepartmentName = technicianDepartmentName;
    }

    public String getUserProject() {
        return UserProject;
    }

    public void setUserProject(String userProject) {
        UserProject = userProject;
    }

    public String getUserBuilding() {
        return UserBuilding;
    }

    public void setUserBuilding(String userBuilding) {
        UserBuilding = userBuilding;
    }

    public String getUserFlat() {
        return UserFlat;
    }

    public void setUserFlat(String userFlat) {
        UserFlat = userFlat;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserMobile() {
        return UserMobile;
    }

    public void setUserMobile(String userMobile) {
        UserMobile = userMobile;
    }

    public String getWorkorder_created_time() {
        return workorder_created_time;
    }

    public void setWorkorder_created_time(String workorder_created_time) {
        this.workorder_created_time = workorder_created_time;
    }
}
