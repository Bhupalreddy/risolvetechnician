package self.sampleapp.model;

/**
 * Created by PsSri on 8/8/2017.
 */

public class PostEstimation {
    /*
          "estimation": [
            {
              "service": "serv1",
              "serviceAmount": "100",
              "qty": "",
              "goods": "",
              "unitPrice": "",
              "goodsAmount": ""
            },
            {
              "service": "ser2",
              "serviceAmount": "100",
              "qty": "",
              "goods": "",
              "unitPrice": "",
              "goodsAmount": ""
            },
            {
              "service": "",
              "serviceAmount": "",
              "qty": "2",
              "goods": "pipe",
              "unitPrice": "300",
              "goodsAmount": "600"
            }
          ],
     */
    public String service;
    public double serviceAmount;
    public int qty;
    public String goods;
    public double unitPrice;
    public double goodsAmount;
}
