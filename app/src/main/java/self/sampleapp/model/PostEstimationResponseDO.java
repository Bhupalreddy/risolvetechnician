
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostEstimationResponseDO {

    @SerializedName("msg_code")
    @Expose
    private Integer msgCode;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("output")
    @Expose
    private String output;

    @SerializedName("complaint_id")
    @Expose
    private String complaint_id;

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getComplaint_id() {
        return complaint_id;
    }

    public void setComplaint_id(String complaint_id) {
        this.complaint_id = complaint_id;
    }
}
