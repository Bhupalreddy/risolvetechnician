package self.sampleapp.model;

/**
 * Created by 259173 on 7/14/2017.
 */

public class EstimateMaterial {
    public String itemId;
    public int quantity;
    public String itemName;
    public double unitPrice;
    public double amount;
    public boolean isDeleted=false;
}
