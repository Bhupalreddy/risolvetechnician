
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WorkOrders {

    @SerializedName("complaints")
    @Expose
    private ArrayList<WorkOrder> complaints = null;
    @SerializedName("msg_code")
    @Expose
    private Integer msgCode;
    @SerializedName("msg")
    @Expose
    private String msg;

    public ArrayList<WorkOrder> getComplaints() {
        return complaints;
    }

    public void setComplaints(ArrayList<WorkOrder> complaints) {
        this.complaints = complaints;
    }

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
