
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Estimation implements Serializable{
    @SerializedName("estimation_id")
    @Expose
    private String estimation_id;
    @SerializedName("estimation_description_of_work")
    @Expose
    private String estimation_description_of_work;
    @SerializedName("estimation_pdf")
    @Expose
    private String estimation_pdf;
    @SerializedName("estimation_complaint_id")
    @Expose
    private String estimation_complaint_id;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("estimation_technician_id")
    @Expose
    private String estimation_technician_id;
    @SerializedName("complaint_id")
    @Expose
    private String complaintId;
    @SerializedName("complaint_number")
    @Expose
    private String complaintNumber;
    @SerializedName("complaint_user_id")
    @Expose
    private String complaintUserId;
    @SerializedName("complaint_subject")
    @Expose
    private String complaintSubject;
    @SerializedName("complaint_description")
    @Expose
    private String complaintDescription;
    @SerializedName("complaint_department_id")
    @Expose
    private String complaintDepartmentId;
    @SerializedName("complaint_images")
    @Expose
    private String complaintImages;
    @SerializedName("complaint_videos")
    @Expose
    private String complaintVideos;
    @SerializedName("complaint_availabulity_time1")
    @Expose
    private String complaintAvailabulityTime1;
    @SerializedName("complaint_availabulity_time2")
    @Expose
    private String complaintAvailabulityTime2;
    @SerializedName("complaint_acceptence_id")
    @Expose
    private Object complaintAcceptenceId;
    @SerializedName("complaint_user_feedback")
    @Expose
    private Object complaintUserFeedback;
    @SerializedName("complaint_technician_review")
    @Expose
    private Object complaintTechnicianReview;
    @SerializedName("complaint_technician_uplode_images")
    @Expose
    private Object complaintTechnicianUplodeImages;
    @SerializedName("complaint_user_signature")
    @Expose
    private Object complaintUserSignature;
    @SerializedName("complaint_estimated_id")
    @Expose
    private Object complaintEstimatedId;
    @SerializedName("complaint_assigned_id")
    @Expose
    private Object complaintAssignedId;
    @SerializedName("complaint_created_date")
    @Expose
    private String complaintCreatedDate;
    @SerializedName("complaint_updated_date")
    @Expose
    private String complaintUpdatedDate;
    @SerializedName("complaint_status")
    @Expose
    private String complaintStatus;
    @SerializedName("TechnicianName")
    @Expose
    private String TechnicianName;
    @SerializedName("TechnicianMobile")
    @Expose
    private String TechnicianMobile;
    @SerializedName("TechnicianImage")
    @Expose
    private String TechnicianImage;
    @SerializedName("TechnicianDepartmentName")
    @Expose
    private String TechnicianDepartmentName;
    @SerializedName("UserProject")
    @Expose
    private String UserProject;
    @SerializedName("UserBuilding")
    @Expose
    private String UserBuilding;
    @SerializedName("UserFlat")
    @Expose
    private String UserFlat;
    @SerializedName("UserName")
    @Expose
    private String UserName;
    @SerializedName("UserMobile")
    @Expose
    private String UserMobile;
    @SerializedName("complaint_created_time")
    @Expose
    private String complaint_created_time;
    @SerializedName("bma_department_image")
    @Expose
    private String bma_department_image;


    public String getEstimation_id() {
        return estimation_id;
    }

    public void setEstimation_id(String estimation_id) {
        this.estimation_id = estimation_id;
    }

    public String getEstimation_description_of_work() {
        return estimation_description_of_work;
    }

    public void setEstimation_description_of_work(String estimation_description_of_work) {
        this.estimation_description_of_work = estimation_description_of_work;
    }

    public String getEstimation_pdf() {
        return estimation_pdf;
    }

    public void setEstimation_pdf(String estimation_pdf) {
        this.estimation_pdf = estimation_pdf;
    }

    public String getEstimation_complaint_id() {
        return estimation_complaint_id;
    }

    public void setEstimation_complaint_id(String estimation_complaint_id) {
        this.estimation_complaint_id = estimation_complaint_id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getEstimation_technician_id() {
        return estimation_technician_id;
    }

    public void setEstimation_technician_id(String estimation_technician_id) {
        this.estimation_technician_id = estimation_technician_id;
    }

    public String getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(String complaintId) {
        this.complaintId = complaintId;
    }

    public String getComplaintNumber() {
        return complaintNumber;
    }

    public void setComplaintNumber(String complaintNumber) {
        this.complaintNumber = complaintNumber;
    }

    public String getComplaintUserId() {
        return complaintUserId;
    }

    public void setComplaintUserId(String complaintUserId) {
        this.complaintUserId = complaintUserId;
    }

    public String getComplaintSubject() {
        return complaintSubject;
    }

    public void setComplaintSubject(String complaintSubject) {
        this.complaintSubject = complaintSubject;
    }

    public String getComplaintDescription() {
        return complaintDescription;
    }

    public void setComplaintDescription(String complaintDescription) {
        this.complaintDescription = complaintDescription;
    }

    public String getComplaintDepartmentId() {
        return complaintDepartmentId;
    }

    public void setComplaintDepartmentId(String complaintDepartmentId) {
        this.complaintDepartmentId = complaintDepartmentId;
    }

    public String getComplaintImages() {
        return complaintImages;
    }

    public void setComplaintImages(String complaintImages) {
        this.complaintImages = complaintImages;
    }

    public String getComplaintVideos() {
        return complaintVideos;
    }

    public void setComplaintVideos(String complaintVideos) {
        this.complaintVideos = complaintVideos;
    }

    public String getComplaintAvailabulityTime1() {
        return complaintAvailabulityTime1;
    }

    public void setComplaintAvailabulityTime1(String complaintAvailabulityTime1) {
        this.complaintAvailabulityTime1 = complaintAvailabulityTime1;
    }

    public String getComplaintAvailabulityTime2() {
        return complaintAvailabulityTime2;
    }

    public void setComplaintAvailabulityTime2(String complaintAvailabulityTime2) {
        this.complaintAvailabulityTime2 = complaintAvailabulityTime2;
    }

    public Object getComplaintAcceptenceId() {
        return complaintAcceptenceId;
    }

    public void setComplaintAcceptenceId(Object complaintAcceptenceId) {
        this.complaintAcceptenceId = complaintAcceptenceId;
    }

    public Object getComplaintUserFeedback() {
        return complaintUserFeedback;
    }

    public void setComplaintUserFeedback(Object complaintUserFeedback) {
        this.complaintUserFeedback = complaintUserFeedback;
    }

    public Object getComplaintTechnicianReview() {
        return complaintTechnicianReview;
    }

    public void setComplaintTechnicianReview(Object complaintTechnicianReview) {
        this.complaintTechnicianReview = complaintTechnicianReview;
    }

    public Object getComplaintTechnicianUplodeImages() {
        return complaintTechnicianUplodeImages;
    }

    public void setComplaintTechnicianUplodeImages(Object complaintTechnicianUplodeImages) {
        this.complaintTechnicianUplodeImages = complaintTechnicianUplodeImages;
    }

    public Object getComplaintUserSignature() {
        return complaintUserSignature;
    }

    public void setComplaintUserSignature(Object complaintUserSignature) {
        this.complaintUserSignature = complaintUserSignature;
    }

    public Object getComplaintEstimatedId() {
        return complaintEstimatedId;
    }

    public void setComplaintEstimatedId(Object complaintEstimatedId) {
        this.complaintEstimatedId = complaintEstimatedId;
    }

    public Object getComplaintAssignedId() {
        return complaintAssignedId;
    }

    public void setComplaintAssignedId(Object complaintAssignedId) {
        this.complaintAssignedId = complaintAssignedId;
    }

    public String getComplaintCreatedDate() {
        return complaintCreatedDate;
    }

    public void setComplaintCreatedDate(String complaintCreatedDate) {
        this.complaintCreatedDate = complaintCreatedDate;
    }

    public String getComplaintUpdatedDate() {
        return complaintUpdatedDate;
    }

    public void setComplaintUpdatedDate(String complaintUpdatedDate) {
        this.complaintUpdatedDate = complaintUpdatedDate;
    }

    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public String getTechnicianName() {
        return TechnicianName;
    }

    public void setTechnicianName(String technicianName) {
        TechnicianName = technicianName;
    }

    public String getTechnicianMobile() {
        return TechnicianMobile;
    }

    public void setTechnicianMobile(String technicianMobile) {
        TechnicianMobile = technicianMobile;
    }

    public String getTechnicianImage() {
        return TechnicianImage;
    }

    public void setTechnicianImage(String technicianImage) {
        TechnicianImage = technicianImage;
    }

    public String getTechnicianDepartmentName() {
        return TechnicianDepartmentName;
    }

    public void setTechnicianDepartmentName(String technicianDepartmentName) {
        TechnicianDepartmentName = technicianDepartmentName;
    }

    public String getUserProject() {
        return UserProject;
    }

    public void setUserProject(String userProject) {
        UserProject = userProject;
    }

    public String getUserBuilding() {
        return UserBuilding;
    }

    public void setUserBuilding(String userBuilding) {
        UserBuilding = userBuilding;
    }

    public String getUserFlat() {
        return UserFlat;
    }

    public void setUserFlat(String userFlat) {
        UserFlat = userFlat;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserMobile() {
        return UserMobile;
    }

    public void setUserMobile(String userMobile) {
        UserMobile = userMobile;
    }

    public String getComplaint_created_time() {
        return complaint_created_time;
    }

    public void setComplaint_created_time(String complaint_created_time) {
        this.complaint_created_time = complaint_created_time;
    }

    public String getBma_department_image() {
        return bma_department_image;
    }

    public void setBma_department_image(String bma_department_image) {
        this.bma_department_image = bma_department_image;
    }
}
