
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EstimatedImageUploadRequestDO {

    @SerializedName("msg_code")
    @Expose
    private Integer msgCode;
    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("output")
    @Expose
    private String output;
    @SerializedName("images")
    @Expose
    private String images;

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
