
package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Estimations {

    @SerializedName("complaints")
    @Expose
    private ArrayList<Estimation> complaints = null;
    @SerializedName("msg_code")
    @Expose
    private Integer msgCode;
    @SerializedName("msg")
    @Expose
    private String msg;

    public ArrayList<Estimation> getComplaints() {
        return complaints;
    }

    public void setComplaints(ArrayList<Estimation> complaints) {
        this.complaints = complaints;
    }

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
