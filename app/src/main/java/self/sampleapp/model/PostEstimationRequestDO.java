package self.sampleapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by PsSri on 8/8/2017.
 */

public class PostEstimationRequestDO {

    @SerializedName("tech_id")
    @Expose
    private String tech_id;
    @SerializedName("complaint_id")
    @Expose
    private String complaint_id;
    @SerializedName("totalservicecast")
    @Expose
    private String totalservicecast;
    @SerializedName("totalgoodscast")
    @Expose
    private String totalgoodscast;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("estimation")
    @Expose
    private ArrayList<PostEstimation> estimation;
    @SerializedName("images")
    @Expose
    private String images;

    public String getTech_id() {
        return tech_id;
    }

    public void setTech_id(String tech_id) {
        this.tech_id = tech_id;
    }

    public String getComplaint_id() {
        return complaint_id;
    }

    public void setComplaint_id(String complaint_id) {
        this.complaint_id = complaint_id;
    }

    public String getTotalservicecast() {
        return totalservicecast;
    }

    public void setTotalservicecast(String totalservicecast) {
        this.totalservicecast = totalservicecast;
    }

    public String getTotalgoodscast() {
        return totalgoodscast;
    }

    public void setTotalgoodscast(String totalgoodscast) {
        this.totalgoodscast = totalgoodscast;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public ArrayList<PostEstimation> getEstimation() {
        return estimation;
    }

    public void setEstimation(ArrayList<PostEstimation> estimation) {
        this.estimation = estimation;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
