package self.sampleapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import self.sampleapp.adapters.PromotionsPagerAdapter;
import self.sampleapp.model.EditProfileResponse;
import self.sampleapp.web.WebAPI;
import self.sampleapp.widget.AutoScrollViewPager;

import static okhttp3.RequestBody.create;

public class ProfileActivity extends AppCompatActivity implements IPickResult {

    @Inject
    WebAPI mWebAPI;
    @Inject
    SharedPreferences sPref;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etPhone) EditText etPhone;
    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.etCommunity) EditText etCommunity;
    @BindView(R.id.btnEdit)
    Button btnEdit;
    @BindView(R.id.ivPic)
    CircularImageView ivPic;
    @BindView(R.id.logout)
    Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_fragment);

        ButterKnife.bind(this);
        ((App) getApplication()).getNetComponent().inject(this);;

        etName.setText(sPref.getString("bmaUserName", "NA"));
        etPhone.setText(sPref.getString("bmaUserMobile", "NA"));
        etEmail.setText(sPref.getString("bmaUserEmail", "NA"));
        etCommunity.setText(sPref.getString("bma_project_name", "NA"));
        btnEdit.setOnClickListener((x) -> processAction(x));
        logout.setOnClickListener(v -> logoutPopup());

        String imgUrl = sPref.getString("bma_user_image", null);
        Picasso.with(ProfileActivity.this)
                .load(imgUrl)
                .placeholder(R.drawable.pic)
                .error(R.drawable.pic)
                .into(ivPic);

    }

    private void processImagePic(View v){
        PickImageDialog.build(new PickSetup())
                .setOnPickResult(ProfileActivity.this).show(getSupportFragmentManager());
    }

    private void processAction(View v){
        String actionName = ((Button)v).getText().toString();
        boolean isEdit = "Edit".equalsIgnoreCase(actionName);
        if(!isEdit){
            editProfileDetails();
        }
        updateUIActions(isEdit);
    }

    private void editProfileDetails(){

        String name = etName.getText().toString();
        String phone = etPhone.getText().toString();

        String message = null;
        if(TextUtils.isEmpty(name)){
            message = "Name shouldn't be empty.";
        }
        else if(TextUtils.isEmpty(phone)){
            message = "Phone number shouldn't be empty.";
        }
        else if(phone.length() != 10){
            message = "Phone number should be 10 digit number.";
        }

        if(message != null){
            Snackbar.make(ivPic, message, Snackbar.LENGTH_SHORT).show();
            updateUIActions(false);
            return;
        }
        MediaType mediaType = MediaType.parse("");

        RequestBody rbName = create(mediaType, name);
        RequestBody rbPhone = create(mediaType, phone);
        RequestBody rbId = create(mediaType, sPref.getString("bmaUserId", ""));
        MultipartBody.Part pImage = null;
        if(r != null){
            File file = new File(r.getPath());
            RequestBody fileBody = RequestBody.create(mediaType, file);
            pImage = MultipartBody.Part.createFormData("bma_user_image", file.getName(), fileBody);
        }

        showProgress("Submitting User Details...");
        Observable<EditProfileResponse> call = mWebAPI.editProfile(rbName, rbPhone, rbId, pImage);
        Disposable callComplaints = call
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> processResponse(x),
                        throwable -> onError(throwable));
    }

    private void onError(Throwable throwable) {
        hideProgress();
        Snackbar.make(ivPic, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    private void processResponse(EditProfileResponse response){
        hideProgress();
        SharedPreferences.Editor e = sPref.edit();
        e.putString("bmaUserName", etName.getText().toString());
        e.putString("bmaUserMobile", etPhone.getText().toString());
        e.putString("bma_user_image", response.getImageUrl());
        e.commit();
        Snackbar.make(ivPic, response.getMsg(), Snackbar.LENGTH_SHORT).show();
    }

    private void updateUIActions(boolean isEdit){
        if(isEdit){
            updateUI(true);
            btnEdit.setText("Submit");
            ivPic.setOnClickListener((x) -> processImagePic(x));
        }
        else{
            updateUI(false);
            btnEdit.setText("Edit");
            ivPic.setOnClickListener(null);
        }
    }

    private PickResult r;

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            Picasso.with(ProfileActivity.this)
                    .load(r.getUri())
                    .placeholder(R.drawable.pic)
                    .error(R.drawable.pic)
                    .into(ivPic);
            this.r = r;
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Snackbar.make(ivPic, r.getError().getMessage(), Snackbar.LENGTH_SHORT).show();
        }
    }

    private void updateUI(boolean enable){
        etName.setEnabled(enable);
        etPhone.setEnabled(enable);
    }

    protected ProgressDialog pDialog;
    public void showProgress(String message){
        if(ProfileActivity.this == null)
            return;
        if(pDialog != null && pDialog.isShowing()){
            pDialog.hide();
            pDialog = null;
        }
        pDialog = new ProgressDialog(ProfileActivity.this);
        pDialog.setMessage(message);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void hideProgress(){
        if(ProfileActivity.this == null)
            return;
        if(pDialog != null && pDialog.isShowing())
            pDialog.hide();
        pDialog = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgress();
    }

    public void logoutPopup() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure you want to Logout?");

        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                logout();
            }
        });

        alertDialogBuilder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void logout() {
        SharedPreferences.Editor e = sPref.edit();
        e.putString("bmaUserId", "");
        e.putString("bmaUserName", "");
        e.putString("bmaUserEmail", "");
        e.putString("bmaUserMobile", "");
        e.putString("bmaProjectAddress", "");
        e.putString("bmaProjectImage", "");
        e.putString("bmaProjectId", "");
        e.putString("bmaUserType", "");

        e.commit();
        Intent in = new Intent(ProfileActivity.this, AuthenticateActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);
        finish();
    }
}
