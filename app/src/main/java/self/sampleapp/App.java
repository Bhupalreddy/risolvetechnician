package self.sampleapp;

import android.app.Application;

import self.sampleapp.web.AppModule;
import self.sampleapp.web.DaggerNetComponent;
import self.sampleapp.web.NetComponent;
import self.sampleapp.web.NetModule;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by srikrishna on 02-07-2017.
 */

public class App extends Application {

    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        // https://github.com/chrisjenx/Calligraphy
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // Dagger%COMPONENT_NAME%
        mNetComponent = DaggerNetComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this)) // This also corresponds to the name of your module: %component_name%Module
//                .netModule(new NetModule("http://kothwal.in/"))
                .netModule(new NetModule("http://digisoftbiz.ae/risolve/"))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }

}
